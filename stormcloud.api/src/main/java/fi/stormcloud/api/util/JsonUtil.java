package fi.stormcloud.api.util;

public class JsonUtil {

        public static final String JSON_PATH_SELF = "self";

        // USER
        public static final String JSON_PATH_EMAIL = "email";
        public static final String JSON_PATH_ENABLE = "enable";
        public static final String JSON_PATH_USERNAME = "username";
        public static final String JSON_PATH_PASSWORD = "password";
        public static final String JSON_PATH_ID = "id";

        // ADMIN
        public static final String JSON_PATH_FIRST_NAME = "firstName";
        public static final String JSON_PATH_LAST_NAME = "lastName";

        // APIKEY
        public static final String JSON_PATH_API_ID = "apiId";
        public static final String JSON_PATH_API_SECRET = "apiSecret";

        // DEVICE
        public static final String JSON_PATH_SERIAL = "serial";
        public static final String JSON_PATH_TAG = "tag";
        public static final String JSON_PATH_DESCRIPTION = "description";

        // HTTP MESSAGE
        public static final String JSON_PATH_STATUS = "status";
        public static final String JSON_PATH_MESSAGE = "message";

}
