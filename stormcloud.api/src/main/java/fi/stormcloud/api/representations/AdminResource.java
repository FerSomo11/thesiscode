package fi.stormcloud.api.representations;

import org.springframework.hateoas.ResourceSupport;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AdminResource extends ResourceSupport {
        private String adminId;
        private String username;
        private String password;
        private String email;
        private String firstName;
        private String lastName;
        private Boolean enable;

        private String createdBy;
        private String modifiedBy;

        private String createdAt;
        private String modifiedAt;

        public String getAdminId() {
                return adminId;
        }

        public void setAdminId(String id) {
                this.adminId = id;
        }

        public Boolean getEnable() {
                return enable != null && enable;
        }

        public void setEnable(Boolean enable) {
                this.enable = enable != null && enable;
        }

        public String getUsername() {
                return username;
        }

        public void setUsername(String username) {
                this.username = username;
        }

        public String getPassword() {
                return password;
        }

        public void setPassword(String password) {
                this.password = password;
        }

        public String getEmail() {
                return email;
        }

        public void setEmail(String email) {
                this.email = email;
        }

        public String getFirstName() {
                return firstName;
        }

        public void setFirstName(String firstName) {
                this.firstName = firstName;
        }

        public String getLastName() {
                return lastName;
        }

        public void setLastName(String lastName) {
                this.lastName = lastName;
        }

        public String getCreatedBy() {
                return createdBy;
        }

        public void setCreatedBy(String createdBy) {
                this.createdBy = createdBy;
        }

        public String getModifiedBy() {
                return modifiedBy;
        }

        public void setModifiedBy(String modifiedBy) {
                this.modifiedBy = modifiedBy;
        }

        public String getCreatedAt() {
                return createdAt;
        }

        public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
        }

        public String getModifiedAt() {
                return modifiedAt;
        }

        public void setModifiedAt(String modifiedAt) {
                this.modifiedAt = modifiedAt;
        }
}
