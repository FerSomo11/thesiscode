package fi.stormcloud.api.representations;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import org.springframework.stereotype.Component;

import fi.stormcloud.api.controller.DeviceController;
import fi.stormcloud.core.model.Device;

@Component
public class DeviceResourceAssembler extends ResourceAssemblerSupport<Device, DeviceResource> {

        public DeviceResourceAssembler() {
                super(DeviceController.class, DeviceResource.class);
        }

        @Override
        public DeviceResource toResource(Device device) {
                // Creates a representation of Device with a link with rel self to itself.
                DeviceResource resource = createResourceWithId(device.getDeviceId(), device);
                resource.add(linkTo(DeviceController.class).slash(device.getDeviceId()).withRel("update"));
                resource.add(linkTo(DeviceController.class).slash(device.getDeviceId()).withRel("delete"));
                resource.add(linkTo(DeviceController.class).slash(device.getDeviceId()).slash("enable").withRel("enable"));
                resource.add(linkTo(DeviceController.class).slash(device.getDeviceId()).slash("disable").withRel("disable"));
                return resource;
        }

        @Override
        protected DeviceResource instantiateResource(Device device) {
                DeviceResource resource = new DeviceResource();
                resource.setDeviceId(device.getDeviceId());
                resource.setSerial(device.getSerial());
                resource.setEmail(device.getEmail());
                resource.setTag(device.getTag());
                resource.setDescription(device.getDescription());
                resource.setEnable(device.getEnable());
                resource.setCreatedBy(device.getCreatedBy());
                resource.setCreatedAt(device.getCreatedAt());
                resource.setModifiedBy(device.getModifiedBy());
                resource.setModifiedAt(device.getModifiedAt());
                return resource;
        }
}
