package fi.stormcloud.api.representations;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

import org.springframework.stereotype.Component;

import fi.stormcloud.api.controller.AdminController;
import fi.stormcloud.core.model.Admin;

@Component
public class AdminResourceAssembler extends ResourceAssemblerSupport<Admin, AdminResource> {

        public AdminResourceAssembler() {
                super(AdminController.class, AdminResource.class);
        }

        @Override
        public AdminResource toResource(Admin admin) {
                //Creates a representation of Admin with a link with rel self to itself.
                AdminResource resource = createResourceWithId(admin.getAdminId(), admin);
                resource.add(linkTo(AdminController.class).slash(admin.getAdminId()).withRel("update"));
                resource.add(linkTo(AdminController.class).slash(admin.getAdminId()).withRel("delete"));
                resource.add(linkTo(AdminController.class).slash(admin.getAdminId()).slash("enable").withRel("enable"));
                resource.add(linkTo(AdminController.class).slash(admin.getAdminId()).slash("disable").withRel("disable"));
                return resource;
        }
        
        @Override
        protected AdminResource instantiateResource(Admin admin) {
                AdminResource resource = new AdminResource();
                resource.setAdminId(admin.getAdminId());
                resource.setUsername(admin.getUsername());
                resource.setPassword(admin.getPassword());
                resource.setEmail(admin.getEmail());
                resource.setFirstName(admin.getFirstName());
                resource.setLastName(admin.getLastName());
                resource.setEnable(admin.getEnable());
                resource.setCreatedBy(admin.getCreatedBy());
                resource.setCreatedAt(admin.getCreatedAt());
                resource.setModifiedBy(admin.getModifiedBy());
                resource.setModifiedAt(admin.getModifiedAt());
                return resource;
        }
        

}
