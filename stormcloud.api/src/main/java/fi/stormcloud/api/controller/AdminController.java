package fi.stormcloud.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fi.stormcloud.api.representations.AdminResource;
import fi.stormcloud.api.representations.AdminResourceAssembler;
import fi.stormcloud.core.exception.AuthenticationException;
import fi.stormcloud.core.exception.account.AccountException;
import fi.stormcloud.core.exception.account.AccountNotFoundException;
import fi.stormcloud.core.exception.group.GroupException;
import fi.stormcloud.core.model.Admin;
import fi.stormcloud.services.service.AdminService;

@RestController
@RequestMapping(value = AdminController.PATH)
public class AdminController extends BaseController {
        public static final String PATH = "/admins";

        private AdminService adminService;
        
        @Autowired
        private AdminResourceAssembler adminResourceAssembler;

        public AdminController() {
                adminService = new AdminService();
        }

        @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
        @ResponseBody
        public HttpEntity<AdminResource> create(@RequestBody Admin admin) throws AccountException, GroupException, AuthenticationException {
                admin = adminService.create(getSubjectApplication(), admin);
                AdminResource adminResource = adminResourceAssembler.toResource(admin);
                return new ResponseEntity<AdminResource>(adminResource, HttpStatus.CREATED);
        }

        @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
        @ResponseBody
        public HttpEntity<List<AdminResource>> list() throws GroupException, AccountException, AuthenticationException {
                List<Admin> admins = adminService.list(getSubjectApplication());
                List<AdminResource> adminsResources = adminResourceAssembler.toResources(admins);
                return new ResponseEntity<List<AdminResource>>(adminsResources, HttpStatus.ACCEPTED);
        }

        @RequestMapping(value = "/{adminId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
        @ResponseBody
        public HttpEntity<AdminResource> read(@PathVariable(value = "adminId") String id) throws AccountNotFoundException, AuthenticationException {
                Admin admin = adminService.read(getSubjectApplication(), id);
                AdminResource adminResource = adminResourceAssembler.toResource(admin);
                return new ResponseEntity<AdminResource>(adminResource, HttpStatus.FOUND);
        }

        @RequestMapping(value = "/{adminId}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
        @ResponseBody
        public HttpEntity<AdminResource> update(@PathVariable(value = "adminId") String id, @RequestBody Admin admin) throws AccountException, AuthenticationException {
                admin = adminService.update(getSubjectApplication(), id, admin);
                AdminResource adminResource = adminResourceAssembler.toResource(admin);
                return new ResponseEntity<AdminResource>(adminResource, HttpStatus.ACCEPTED);
        }

        @RequestMapping(value = "/{adminId}", method = RequestMethod.PATCH, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
        @ResponseBody
        public HttpEntity<AdminResource> partialUpdate(@PathVariable(value = "adminId") String id, @RequestBody Admin admin) throws AccountException, AuthenticationException {
                admin = adminService.partialUpdate(getSubjectApplication(), id, admin);
                AdminResource adminResource = adminResourceAssembler.toResource(admin);
                return new ResponseEntity<AdminResource>(adminResource, HttpStatus.ACCEPTED);
        }

        @RequestMapping(value = "/{adminId}/enable", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
        @ResponseBody
        public HttpEntity<AdminResource> enable(@PathVariable(value = "adminId") String id) throws AccountException, AuthenticationException {
                Admin admin = adminService.enable(getSubjectApplication(), id);
                AdminResource adminResource = adminResourceAssembler.toResource(admin);
                return new ResponseEntity<AdminResource>(adminResource, HttpStatus.ACCEPTED);
        }

        @RequestMapping(value = "/{adminId}/disable", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
        @ResponseBody
        public HttpEntity<AdminResource> disable(@PathVariable(value = "adminId") String id) throws AccountException, AuthenticationException {
                Admin admin = adminService.disable(getSubjectApplication(), id);
                AdminResource adminResource = adminResourceAssembler.toResource(admin);
                return new ResponseEntity<AdminResource>(adminResource, HttpStatus.ACCEPTED);
        }

        @RequestMapping(value = "/{adminId}", method = RequestMethod.DELETE, produces = MediaType.TEXT_PLAIN_VALUE)
        @ResponseBody
        public HttpEntity<String> delete(@PathVariable(value = "adminId") String id) throws AccountException, AuthenticationException {
                adminService.delete(getSubjectApplication(), id);
                return new ResponseEntity<String>(HttpStatus.GONE.getReasonPhrase(), HttpStatus.GONE);
        }

        @RequestMapping(value = "/{adminId}/apikey", method = RequestMethod.POST, produces = MediaType.TEXT_PLAIN_VALUE)
        @ResponseBody
        public HttpEntity<String> createApiKey(@PathVariable(value = "adminId") String id) throws AccountException, AuthenticationException {
                adminService.createApiKey(getSubjectApplication(), id);
                return new ResponseEntity<String>(HttpStatus.CREATED.getReasonPhrase(), HttpStatus.CREATED);
        }

        @RequestMapping(value = "/{adminId}/apikey/{apiId}/enable", method = RequestMethod.POST, produces = MediaType.TEXT_PLAIN_VALUE)
        @ResponseBody
        public HttpEntity<String> enable(@PathVariable(value = "adminId") String id, @PathVariable(value = "apiId") String apiId) throws AccountException, AuthenticationException {
                adminService.enableApiKey(getSubjectApplication(), id, apiId);
                return new ResponseEntity<String>(HttpStatus.ACCEPTED.getReasonPhrase(), HttpStatus.ACCEPTED);
        }

        @RequestMapping(value = "/{adminId}/apikey/{apiId}/disable", method = RequestMethod.POST, produces = MediaType.TEXT_PLAIN_VALUE)
        @ResponseBody
        public HttpEntity<String> disable(@PathVariable(value = "adminId") String id, @PathVariable(value = "apiId") String apiId) throws AccountException, AuthenticationException {
                adminService.disableApiKey(getSubjectApplication(), id, apiId);
                return new ResponseEntity<String>(HttpStatus.ACCEPTED.getReasonPhrase(), HttpStatus.ACCEPTED);
        }

        @RequestMapping(value = "/{adminId}/apikey/{apiId}", method = RequestMethod.DELETE, produces = MediaType.TEXT_PLAIN_VALUE)
        @ResponseBody
        public HttpEntity<String> delete(@PathVariable(value = "adminId") String id, @PathVariable(value = "apiId") String apiId) throws AccountException, AuthenticationException {
                adminService.deleteApiKey(getSubjectApplication(), id, apiId);
                return new ResponseEntity<String>(HttpStatus.GONE.getReasonPhrase(), HttpStatus.GONE);
        }
}
