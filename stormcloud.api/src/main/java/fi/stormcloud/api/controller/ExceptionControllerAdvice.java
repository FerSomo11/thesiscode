package fi.stormcloud.api.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import fi.stormcloud.core.exception.AuthenticationException;
import fi.stormcloud.core.exception.account.AccountAlreadyExistsException;
import fi.stormcloud.core.exception.account.AccountMissingFieldsException;
import fi.stormcloud.core.exception.account.AccountNotFoundException;
import fi.stormcloud.core.exception.apikey.ApiKeyNotFoundException;
import fi.stormcloud.core.exception.data.DataMissingFieldsException;
import fi.stormcloud.core.exception.group.GroupNotFoundException;

@ControllerAdvice
public class ExceptionControllerAdvice {

        @ExceptionHandler(Exception.class)
        @RequestMapping(produces = MediaType.TEXT_PLAIN_VALUE)
        @ResponseBody
        public HttpEntity<String> handleException(Exception e) {
                return handleException(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        @ExceptionHandler(GroupNotFoundException.class)
        @RequestMapping(produces = MediaType.TEXT_PLAIN_VALUE)
        @ResponseBody
        public HttpEntity<String> handleGroupNotFoundException(Exception e) {
                return handleException(HttpStatus.CONFLICT);
        }

        @ExceptionHandler(AccountNotFoundException.class)
        @RequestMapping(produces = MediaType.TEXT_PLAIN_VALUE)
        @ResponseBody
        public HttpEntity<String> handleAccountNotFoundException(Exception e) {
                return handleException(HttpStatus.NOT_FOUND);
        }

        @ExceptionHandler(AccountMissingFieldsException.class)
        @RequestMapping(produces = MediaType.TEXT_PLAIN_VALUE)
        @ResponseBody
        public HttpEntity<String> handleAccountMissingFieldsException(Exception e) {
                return handleException(HttpStatus.FORBIDDEN);
        }

        @ExceptionHandler(AccountAlreadyExistsException.class)
        @RequestMapping(produces = MediaType.TEXT_PLAIN_VALUE)
        @ResponseBody
        public HttpEntity<String> handleAccountAlreadyExistsException(Exception e) {
                return handleException(HttpStatus.UNPROCESSABLE_ENTITY);
        }

        @ExceptionHandler(ApiKeyNotFoundException.class)
        @RequestMapping(produces = MediaType.TEXT_PLAIN_VALUE)
        @ResponseBody
        public HttpEntity<String> handleApiKeyNotFoundException(Exception e) {
                return handleException(HttpStatus.NOT_FOUND);
        }

        @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
        @RequestMapping(produces = MediaType.TEXT_PLAIN_VALUE)
        @ResponseBody
        public HttpEntity<String> handleHttpRequestMethodNotSupportedException(Exception e) {
                return handleException(HttpStatus.METHOD_NOT_ALLOWED);
        }

        @ExceptionHandler(AuthenticationException.class)
        @RequestMapping(produces = MediaType.TEXT_PLAIN_VALUE)
        @ResponseBody
        public HttpEntity<String> handleAuthenticationException(Exception e) {
                return handleException(HttpStatus.UNAUTHORIZED);
        }
        
        @ExceptionHandler(DataMissingFieldsException.class)
        @RequestMapping(produces = MediaType.TEXT_PLAIN_VALUE)
        @ResponseBody
        public HttpEntity<String> handlePullMissingFieldsException(Exception e) {
                return handleException(HttpStatus.FORBIDDEN);
        }

        private HttpEntity<String> handleException(HttpStatus httpStatus) {
                return new ResponseEntity<String>(httpStatus.getReasonPhrase(), httpStatus);
        }
}
