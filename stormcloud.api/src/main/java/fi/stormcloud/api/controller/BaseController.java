package fi.stormcloud.api.controller;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import com.stormpath.sdk.application.Application;

import fi.stormcloud.core.exception.AuthenticationException;
import fi.stormcloud.services.dao.ApplicationDao;
import fi.stormcloud.services.security.authc.WorkspacePrincipalCollection;

public abstract class BaseController {
        public Application getSubjectApplication() throws AuthenticationException {
                if (!SecurityUtils.getSubject().isAuthenticated()) {
                        throw new AuthenticationException();
                }

                Subject subject = SecurityUtils.getSubject();
                WorkspacePrincipalCollection wpc = (WorkspacePrincipalCollection) subject.getPrincipals();
                if (StringUtils.isBlank(wpc.getWorkspace())) {
                        return null;
                }
                return new ApplicationDao().read(wpc.getWorkspace());
        }
}
