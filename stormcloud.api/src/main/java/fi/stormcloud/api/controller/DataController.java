package fi.stormcloud.api.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fi.stormcloud.core.exception.AuthenticationException;
import fi.stormcloud.core.exception.data.DataCreatedFailedException;
import fi.stormcloud.core.exception.data.DataMissingFieldsException;
import fi.stormcloud.core.exception.data.DataNotFoundException;
import fi.stormcloud.core.model.Data;
import fi.stormcloud.services.service.DataService;

@RestController
@RequestMapping(value = DataController.PATH)
public class DataController {
        public static final String PATH = "/data";

        private DataService dataService;

        public DataController() {
                dataService = new DataService();
        }

        @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
        @ResponseBody
        public HttpEntity<String> push(@RequestBody String raw) throws AuthenticationException, IOException, DataNotFoundException, DataCreatedFailedException {
                String rawData = dataService.push(raw);
                return new ResponseEntity<String>(rawData, HttpStatus.CREATED);
        }

        @RequestMapping(value = "/timerange", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
        @ResponseBody
        public ResponseEntity<List<String>> pullByTimeRange(@RequestParam(required = false, value = "start") String start, @RequestParam(required = false, value = "end") String end)
                        throws AuthenticationException, DataMissingFieldsException, DataNotFoundException {
                List<String> list = dataService.pullByTimestamp(start, end);
                return new ResponseEntity<List<String>>(list, HttpStatus.ACCEPTED);

        }

        @RequestMapping(value = "/location", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
        @ResponseBody
        public ResponseEntity<List<String>> pullByLocation(@RequestParam(required = false, value = "location") String location, @RequestParam(required = false, value = "distance", defaultValue = "1000") String distance) throws AuthenticationException, DataMissingFieldsException, DataNotFoundException{
                List<String> list = dataService.pullByLocation(location,distance);
                return new ResponseEntity<List<String>>(list, HttpStatus.ACCEPTED);
        }
        
        @RequestMapping(value = "/allData", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
        @ResponseBody
        public ResponseEntity<List<Data>> pullAll() throws AuthenticationException, DataMissingFieldsException, DataNotFoundException{
                List<Data> list = dataService.pullAll();
                return new ResponseEntity<List<Data>>(list, HttpStatus.ACCEPTED);
        }

}
