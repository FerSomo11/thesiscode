package fi.stormcloud.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fi.stormcloud.api.representations.DeviceResource;
import fi.stormcloud.api.representations.DeviceResourceAssembler;
import fi.stormcloud.core.exception.AuthenticationException;
import fi.stormcloud.core.exception.account.AccountException;
import fi.stormcloud.core.exception.account.AccountNotFoundException;
import fi.stormcloud.core.exception.group.GroupException;
import fi.stormcloud.core.model.Device;
import fi.stormcloud.services.service.DeviceService;

@RestController
@RequestMapping(value = DeviceController.PATH)
public class DeviceController extends BaseController {
        public static final String PATH = "/devices";

        private DeviceService deviceService;
        
        @Autowired
        private DeviceResourceAssembler deviceResourceAssembler;

        public DeviceController() {
                deviceService = new DeviceService();
        }

        @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
        @ResponseBody
        public HttpEntity<DeviceResource> create(@RequestBody Device device) throws AccountException, GroupException, AuthenticationException {
                device = deviceService.create(getSubjectApplication(), device);
                DeviceResource deviceResource = deviceResourceAssembler.toResource(device);
                return new ResponseEntity<DeviceResource>(deviceResource, HttpStatus.CREATED);
        }

        @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
        @ResponseBody
        public HttpEntity<List<DeviceResource>> list() throws GroupException, AccountException, AuthenticationException {
                List<Device> list = deviceService.list(getSubjectApplication());
                List<DeviceResource> resourceList = deviceResourceAssembler.toResources(list);
                return new ResponseEntity<List<DeviceResource>>(resourceList, HttpStatus.ACCEPTED);
        }

        @RequestMapping(value = "/{deviceId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
        @ResponseBody
        public HttpEntity<DeviceResource> read(@PathVariable(value = "deviceId") String id) throws AccountNotFoundException, AuthenticationException {
                Device device = deviceService.read(getSubjectApplication(), id);
                DeviceResource deviceResource = deviceResourceAssembler.toResource(device);
                return new ResponseEntity<DeviceResource>(deviceResource, HttpStatus.FOUND);
        }

        @RequestMapping(value = "/{deviceId}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
        @ResponseBody
        public HttpEntity<DeviceResource> update(@PathVariable(value = "deviceId") String id, @RequestBody Device device) throws AccountException, AuthenticationException {
                device = deviceService.update(getSubjectApplication(), id, device);
                DeviceResource deviceResource = deviceResourceAssembler.toResource(device);
                return new ResponseEntity<DeviceResource>(deviceResource, HttpStatus.ACCEPTED);
        }

        @RequestMapping(value = "/{deviceId}", method = RequestMethod.PATCH, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
        @ResponseBody
        public HttpEntity<DeviceResource> partialUpdate(@PathVariable(value = "deviceId") String id, @RequestBody Device device) throws AccountException, AuthenticationException {
                device = deviceService.partialUpdate(getSubjectApplication(), id, device);
                DeviceResource deviceResource = deviceResourceAssembler.toResource(device);
                return new ResponseEntity<DeviceResource>(deviceResource, HttpStatus.ACCEPTED);
        }

        @RequestMapping(value = "/{deviceId}/enable", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
        @ResponseBody
        public HttpEntity<DeviceResource> enable(@PathVariable(value = "deviceId") String id) throws AccountException, AuthenticationException {
                Device device = deviceService.enable(getSubjectApplication(), id);
                DeviceResource deviceResource = deviceResourceAssembler.toResource(device);
                return new ResponseEntity<DeviceResource>(deviceResource, HttpStatus.ACCEPTED);
        }

        @RequestMapping(value = "/{deviceId}/disable", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
        @ResponseBody
        public HttpEntity<DeviceResource> disable(@PathVariable(value = "deviceId") String id) throws AccountException, AuthenticationException {
                Device device = deviceService.disable(getSubjectApplication(), id);
                DeviceResource deviceResource = deviceResourceAssembler.toResource(device);
                return new ResponseEntity<DeviceResource>(deviceResource, HttpStatus.ACCEPTED);
        }

        @RequestMapping(value = "/{deviceId}", method = RequestMethod.DELETE, produces = MediaType.TEXT_PLAIN_VALUE)
        @ResponseBody
        public HttpEntity<String> delete(@PathVariable(value = "deviceId") String id) throws AccountException, AuthenticationException {
                deviceService.delete(getSubjectApplication(), id);
                return new ResponseEntity<String>(HttpStatus.GONE.getReasonPhrase(), HttpStatus.GONE);
        }

        @RequestMapping(value = "/{deviceId}/apikey", method = RequestMethod.POST, produces = MediaType.TEXT_PLAIN_VALUE)
        @ResponseBody
        public HttpEntity<String> createApiKey(@PathVariable(value = "deviceId") String id) throws AccountException, AuthenticationException {
                deviceService.createApiKey(getSubjectApplication(), id);
                return new ResponseEntity<String>(HttpStatus.CREATED.getReasonPhrase(), HttpStatus.CREATED);
        }

        @RequestMapping(value = "/{deviceId}/apikey/{apiId}/enable", method = RequestMethod.POST, produces = MediaType.TEXT_PLAIN_VALUE)
        @ResponseBody
        public HttpEntity<String> enable(@PathVariable(value = "deviceId") String id, @PathVariable(value = "apiId") String apiId) throws AccountException, AuthenticationException {
                deviceService.enableApiKey(getSubjectApplication(), id, apiId);
                return new ResponseEntity<String>(HttpStatus.ACCEPTED.getReasonPhrase(), HttpStatus.ACCEPTED);
        }

        @RequestMapping(value = "/{deviceId}/apikey/{apiId}/disable", method = RequestMethod.POST, produces = MediaType.TEXT_PLAIN_VALUE)
        @ResponseBody
        public HttpEntity<String> disable(@PathVariable(value = "deviceId") String id, @PathVariable(value = "apiId") String apiId) throws AccountException, AuthenticationException {
                deviceService.disableApiKey(getSubjectApplication(), id, apiId);
                return new ResponseEntity<String>(HttpStatus.ACCEPTED.getReasonPhrase(), HttpStatus.ACCEPTED);
        }

        @RequestMapping(value = "/{deviceId}/apikey/{apiId}", method = RequestMethod.DELETE, produces = MediaType.TEXT_PLAIN_VALUE)
        @ResponseBody
        public HttpEntity<String> delete(@PathVariable(value = "deviceId") String id, @PathVariable(value = "apiId") String apiId) throws AccountException, AuthenticationException {
                deviceService.deleteApiKey(getSubjectApplication(), id, apiId);
                return new ResponseEntity<String>(HttpStatus.GONE.getReasonPhrase(), HttpStatus.GONE);
        }
}
