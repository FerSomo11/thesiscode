package fi.stormcloud.api.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fi.stormcloud.core.exception.AuthenticationException;

@RestController
public class AuxiliarController extends BaseController {
        public static final String ROOT_PATH = "/";
        public static final String ANY_PATH = "/**";
        public static final String UNAUTHORIZED = "/unauthorized";

        @RequestMapping(value = AuxiliarController.ANY_PATH, produces = MediaType.TEXT_PLAIN_VALUE)
        @ResponseBody
        public HttpEntity<String> any() throws AuthenticationException {
                throw new AuthenticationException();
        }

        @RequestMapping(value = AuxiliarController.ROOT_PATH, produces = MediaType.TEXT_PLAIN_VALUE)
        @ResponseBody
        public HttpEntity<String> root() throws AuthenticationException {
                throw new AuthenticationException();
        }

        @RequestMapping(value = AuxiliarController.UNAUTHORIZED, produces = MediaType.TEXT_PLAIN_VALUE)
        @ResponseBody
        public HttpEntity<String> unauthorized() throws AuthenticationException {
                throw new AuthenticationException();
        }
}
