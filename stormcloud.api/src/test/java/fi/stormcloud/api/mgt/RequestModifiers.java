package fi.stormcloud.api.mgt;

import org.apache.shiro.codec.Base64;
import org.apache.shiro.codec.CodecSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.restdriver.serverdriver.RestServerDriver;
import com.github.restdriver.serverdriver.http.Header;
import com.github.restdriver.serverdriver.http.RequestBody;

import fi.stormcloud.api.config.security.filter.CustomAuthenticationFilter;
import fi.stormcloud.api.properties.TestProperties;

public class RequestModifiers {
        private final static Logger logger = LoggerFactory.getLogger(RequestModifiers.class);

        public static Header acceptJsonHeader() {
                return RestServerDriver.header("Accept", "application/json");
        }

        public static Header basicAuthorizationHeader(String username, String password) {
                String token = username + ":" + password;
                String encodedToken = Base64.encodeToString(CodecSupport.toBytes(token));
                return RestServerDriver.header(CustomAuthenticationFilter.AUTHORIZATION_HEADER, CustomAuthenticationFilter.STORMCLOUD_BASIC_SCHEME + " " + encodedToken);
        }

        public static Header apiAuthorizationHeader(String apiId, String apiSecret) {
                String token = apiId + ":" + apiSecret;
                String encodedToken = Base64.encodeToString(CodecSupport.toBytes(token));
                return RestServerDriver.header(CustomAuthenticationFilter.AUTHORIZATION_HEADER, CustomAuthenticationFilter.STORMCLOUD_API_SCHEME + " " + encodedToken);
        }

        public static Header workspaceHeader() {
                return RestServerDriver.header(CustomAuthenticationFilter.STORMCLOUD_WORKSPACE_HEADER, TestProperties.INSTANCE.getWorkspace());
        }

        public static RequestBody body(Object o) {
                return RestServerDriver.body(toJson(o), "application/json");
        }

        private static String toJson(Object o) {
                ObjectMapper objectMapper = new ObjectMapper();
                try {
                        return objectMapper.writeValueAsString(o);
                } catch (JsonProcessingException e) {
                        logger.error(e.getMessage());
                        return null;
                }
        }
}
