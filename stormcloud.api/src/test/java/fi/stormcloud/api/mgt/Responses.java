package fi.stormcloud.api.mgt;

import org.springframework.http.HttpMethod;

import com.github.restdriver.serverdriver.RestServerDriver;
import com.github.restdriver.serverdriver.http.AnyRequestModifier;
import com.github.restdriver.serverdriver.http.response.Response;

import fi.stormcloud.api.properties.TestProperties;

public class Responses {

        // private final static Logger logger = LoggerFactory.getLogger(Responses.class);

        public static Response get(String path, AnyRequestModifier... modifiers) {
                return RestServerDriver.get(TestProperties.INSTANCE.getBaseUrl() + path, modifiers);
        }

        public static Response post(String path, AnyRequestModifier... modifiers) {
                return RestServerDriver.post(TestProperties.INSTANCE.getBaseUrl() + path, modifiers);
        }

        public static Response put(String path, AnyRequestModifier... modifiers) {
                return RestServerDriver.put(TestProperties.INSTANCE.getBaseUrl() + path, modifiers);
        }

        public static Response patch(String path, AnyRequestModifier... modifiers) {
                return RestServerDriver.method(HttpMethod.PATCH.name().toLowerCase(), TestProperties.INSTANCE.getBaseUrl() + path, modifiers);
        }

        public static Response delete(String path, AnyRequestModifier... modifiers) {
                return RestServerDriver.delete(TestProperties.INSTANCE.getBaseUrl() + path, modifiers);
        }
}
