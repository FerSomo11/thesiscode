package fi.stormcloud.api.mgt;

import static com.github.restdriver.serverdriver.Matchers.hasHeader;
import static com.github.restdriver.serverdriver.Matchers.hasJsonPath;
import static com.github.restdriver.serverdriver.Matchers.hasStatusCode;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import org.springframework.http.HttpStatus;

import com.github.restdriver.serverdriver.http.response.Response;

import fi.stormcloud.api.util.JsonUtil;
import fi.stormcloud.core.model.Admin;
import fi.stormcloud.core.model.Device;

public class Asserts {

        public static void assertStatusCode(Response response, HttpStatus statusCode) {
                assertThat(response, hasStatusCode(statusCode.value()));
        }

        public static void assertHeaderHasDate(Response response) {
                assertThat(response, hasHeader("Date"));
        }

        public static void assertJsonValueEqualsTo(Response response, String fieldName, String equalArg) {
                assertThat(response.asJson(), hasJsonPath("$." + fieldName, equalTo(equalArg)));
        }

        public static void assertJsonValueEqualsTo(Response response, String fieldName, int equalArg) {
                assertThat(response.asJson(), hasJsonPath("$." + fieldName, equalTo(equalArg)));
        }

        public static void assertJsonValueEqualsTo(Response response, String fieldName, boolean equalArg) {
                assertThat(response.asJson(), hasJsonPath("$." + fieldName, equalTo(equalArg)));
        }

        public static void assertResponseJson(Response response, HttpStatus status) {
                assertStatusCode(response, status);
                assertHeaderHasDate(response);
        }

        public static void assertAdminResponseJson(Response response, HttpStatus status, Admin admin) {
                assertResponseJson(response, status);

                assertJsonValueEqualsTo(response, JsonUtil.JSON_PATH_ID, admin.getAdminId());
                assertJsonValueEqualsTo(response, JsonUtil.JSON_PATH_USERNAME, admin.getUsername());
                assertJsonValueEqualsTo(response, JsonUtil.JSON_PATH_PASSWORD, null);
                assertJsonValueEqualsTo(response, JsonUtil.JSON_PATH_EMAIL, admin.getEmail());
                assertJsonValueEqualsTo(response, JsonUtil.JSON_PATH_FIRST_NAME, admin.getFirstName());
                assertJsonValueEqualsTo(response, JsonUtil.JSON_PATH_LAST_NAME, admin.getLastName());
                assertJsonValueEqualsTo(response, JsonUtil.JSON_PATH_ENABLE, admin.getEnable());
        }

        public static void assertDeviceResponseJson(Response response, HttpStatus status, Device device) {
                assertResponseJson(response, status);

                assertJsonValueEqualsTo(response, JsonUtil.JSON_PATH_ID, device.getDeviceId());
                assertJsonValueEqualsTo(response, JsonUtil.JSON_PATH_SERIAL, device.getSerial());
                assertJsonValueEqualsTo(response, JsonUtil.JSON_PATH_EMAIL, device.getEmail());
                assertJsonValueEqualsTo(response, JsonUtil.JSON_PATH_TAG, device.getTag());
                assertJsonValueEqualsTo(response, JsonUtil.JSON_PATH_DESCRIPTION, device.getDescription());
                assertJsonValueEqualsTo(response, JsonUtil.JSON_PATH_ENABLE, device.getEnable());
        }

        public static void assertMessageResponseJson(Response response, HttpStatus status) {
                assertMessageResponseJson(response, status, status.getReasonPhrase());
        }

        public static void assertMessageResponseJson(Response response, HttpStatus status, String message) {
                assertResponseJson(response, status);
                assertThat(response.getContent(), equalTo(message));
        }

        public static void assertListSize(int listSize, int numberOfElements) {
                assertThat(listSize, equalTo(numberOfElements));
        }}
