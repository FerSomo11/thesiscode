//package fi.stormcloud.api.suite;
//
//import java.util.Arrays;
//import java.util.Collection;
//import java.util.List;
//
//import org.apache.commons.lang3.RandomStringUtils;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runners.Parameterized.Parameters;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.http.HttpStatus;
//
//import com.github.restdriver.serverdriver.http.Header;
//import com.github.restdriver.serverdriver.http.response.Response;
//
//import fi.stormcloud.api.controller.AdminController;
//import fi.stormcloud.api.helper.AdminHelper;
//import fi.stormcloud.api.mgt.Asserts;
//import fi.stormcloud.api.mgt.RequestModifiers;
//import fi.stormcloud.api.mgt.Responses;
//import fi.stormcloud.api.properties.TestProperties;
//import fi.stormcloud.core.model.Admin;
//
//public class AdminTestSuite extends BaseTestSuite {
//        private final static Logger logger = LoggerFactory.getLogger(AdminTestSuite.class);
//
//        private List<Admin> adminList;
//        private Header workspaceHeader;
//        private Header authHeader;
//        private String url;
//
//        public AdminTestSuite(Header authHeader) {
//                this.authHeader = authHeader;
//        }
//
//        @Parameters(name = "{index}: header({0})")
//        public static Collection<Object> data() {
//                Header basicAuthHeader = RequestModifiers.basicAuthorizationHeader(TestProperties.INSTANCE.getAdminUsername(), TestProperties.INSTANCE.getAdminPassword());
//                Header basicApiAuthHeader = RequestModifiers.basicApiAuthorizationHeader(TestProperties.INSTANCE.getAdminApiId(), TestProperties.INSTANCE.getAdminApiSecret());
//                return Arrays.asList(new Object[] { basicAuthHeader, basicApiAuthHeader });
//        }
//
//        @Before
//        public void setUp() {
//                this.workspaceHeader = RequestModifiers.workspaceHeader();
//                this.url = AdminController.PATH + "/";
//                this.adminList = AdminHelper.getAdminList();
//        }
//
//        @Test
//        public void test001() {
//                Admin admin1 = adminList.get(0);
//
//                String random = RandomStringUtils.randomAlphanumeric(15);
//
//                Response response = Responses.get(url + random, workspaceHeader, authHeader);
//                Asserts.assertMessageResponseJson(response, HttpStatus.NOT_FOUND);
//
//                response = Responses.put(url + random, workspaceHeader, authHeader, RequestModifiers.body(admin1));
//                Asserts.assertMessageResponseJson(response, HttpStatus.NOT_FOUND);
//
//                response = Responses.delete(url + random, workspaceHeader, authHeader);
//                Asserts.assertMessageResponseJson(response, HttpStatus.NOT_FOUND);
//        }
//
//        @Test
//        public void test002() {
//                Admin admin1 = adminList.get(0);
//
//                Response response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(admin1));
//                admin1.setId(AdminHelper.toAdmin(response.getContent()).getId());
//                Asserts.assertAdminResponseJson(response, HttpStatus.CREATED, admin1);
//
//                response = Responses.get(url + "/" + admin1.getId(), workspaceHeader, authHeader);
//                Asserts.assertAdminResponseJson(response, HttpStatus.FOUND, admin1);
//
//                response = Responses.put(url + "/" + admin1.getId(), workspaceHeader, authHeader, RequestModifiers.body(admin1));
//                Asserts.assertAdminResponseJson(response, HttpStatus.ACCEPTED, admin1);
//
//                response = Responses.delete(url + "/" + admin1.getId(), workspaceHeader, authHeader);
//                Asserts.assertMessageResponseJson(response, HttpStatus.GONE);
//        }
//
//        @Test
//        public void test003() {
//                Admin admin1 = adminList.get(0);
//
//                Response response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(admin1));
//                admin1.setId(AdminHelper.toAdmin(response.getContent()).getId());
//                Asserts.assertAdminResponseJson(response, HttpStatus.CREATED, admin1);
//
//                response = Responses.get(url + "/" + admin1.getId(), workspaceHeader, authHeader);
//                Asserts.assertAdminResponseJson(response, HttpStatus.FOUND, admin1);
//
//                admin1.setFirstName("DifferentFirstName");
//                admin1.setLastName("DifferentLastName");
//
//                response = Responses.put(url + "/" + admin1.getId(), workspaceHeader, authHeader, RequestModifiers.body(admin1));
//                Asserts.assertAdminResponseJson(response, HttpStatus.ACCEPTED, admin1);
//
//                response = Responses.get(url + "/" + admin1.getId(), workspaceHeader, authHeader);
//                Asserts.assertAdminResponseJson(response, HttpStatus.FOUND, admin1);
//
//                response = Responses.delete(url + "/" + admin1.getId(), workspaceHeader, authHeader);
//                Asserts.assertMessageResponseJson(response, HttpStatus.GONE);
//        }
//
//        @Test
//        public void test004() {
//                Admin admin1 = adminList.get(0);
//
//                Response response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(admin1));
//                admin1.setId(AdminHelper.toAdmin(response.getContent()).getId());
//                Asserts.assertAdminResponseJson(response, HttpStatus.CREATED, admin1);
//
//                response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(admin1));
//                Asserts.assertMessageResponseJson(response, HttpStatus.UNPROCESSABLE_ENTITY);
//
//                response = Responses.delete(url + "/" + admin1.getId(), workspaceHeader, authHeader);
//                Asserts.assertMessageResponseJson(response, HttpStatus.GONE);
//        }
//
//        @Test
//        public void test005() {
//                Admin admin1 = adminList.get(0);
//                Admin admin2 = adminList.get(1);
//
//                Response response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(admin1));
//                admin1.setId(AdminHelper.toAdmin(response.getContent()).getId());
//                Asserts.assertAdminResponseJson(response, HttpStatus.CREATED, admin1);
//
//                response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(admin2));
//                admin2.setId(AdminHelper.toAdmin(response.getContent()).getId());
//                Asserts.assertAdminResponseJson(response, HttpStatus.CREATED, admin2);
//
//                response = Responses.delete(url + "/" + admin1.getId(), workspaceHeader, authHeader);
//                Asserts.assertMessageResponseJson(response, HttpStatus.GONE);
//
//                response = Responses.delete(url + "/" + admin2.getId(), workspaceHeader, authHeader);
//                Asserts.assertMessageResponseJson(response, HttpStatus.GONE);
//        }
//
//        @Test
//        public void test006() {
//                Admin admin1 = adminList.get(0);
//                Admin admin2 = adminList.get(1);
//
//                Response response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(admin1));
//                admin1.setId(AdminHelper.toAdmin(response.getContent()).getId());
//                Asserts.assertAdminResponseJson(response, HttpStatus.CREATED, admin1);
//
//                admin1.setUsername(RandomStringUtils.randomAlphanumeric(15));
//
//                response = Responses.put(url + "/" + admin1.getId(), workspaceHeader, authHeader, RequestModifiers.body(admin1));
//                Asserts.assertAdminResponseJson(response, HttpStatus.ACCEPTED, admin1);
//
//                response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(admin2));
//                admin2.setId(AdminHelper.toAdmin(response.getContent()).getId());
//                Asserts.assertAdminResponseJson(response, HttpStatus.CREATED, admin2);
//
//                admin2.setUsername(admin1.getUsername());
//
//                response = Responses.put(url + "/" + admin2.getId(), workspaceHeader, authHeader, RequestModifiers.body(admin2));
//                Asserts.assertMessageResponseJson(response, HttpStatus.UNPROCESSABLE_ENTITY);
//
//                response = Responses.delete(url + "/" + admin1.getId(), workspaceHeader, authHeader);
//                Asserts.assertMessageResponseJson(response, HttpStatus.GONE);
//
//                response = Responses.delete(url + "/" + admin2.getId(), workspaceHeader, authHeader);
//                Asserts.assertMessageResponseJson(response, HttpStatus.GONE);
//        }
//
//        @Test
//        public void test007() {
//                Admin admin1 = adminList.get(0);
//
//                Response response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(admin1));
//                admin1.setId(AdminHelper.toAdmin(response.getContent()).getId());
//                Asserts.assertAdminResponseJson(response, HttpStatus.CREATED, admin1);
//
//                admin1.setUsername("fakeUsername2");
//                admin1.setPassword("fakePassword2");
//
//                response = Responses.patch(url + "/" + admin1.getId(), workspaceHeader, authHeader, RequestModifiers.body(admin1));
//                Asserts.assertAdminResponseJson(response, HttpStatus.ACCEPTED, admin1);
//
//                response = Responses.get(url + "/" + admin1.getId(), workspaceHeader, authHeader);
//                Asserts.assertAdminResponseJson(response, HttpStatus.FOUND, admin1);
//
//                response = Responses.delete(url + "/" + admin1.getId(), workspaceHeader, authHeader);
//                Asserts.assertMessageResponseJson(response, HttpStatus.GONE);
//        }
//
//        @Test
//        public void test008() {
//                Admin admin1 = adminList.get(0);
//
//                admin1.setUsername(null);
//                Response response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(admin1));
//                Asserts.assertMessageResponseJson(response, HttpStatus.FORBIDDEN);
//        }
//
//        @Test
//        public void test009() {
//                Admin admin1 = adminList.get(0);
//
//                admin1.setPassword(null);
//                Response response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(admin1));
//                Asserts.assertMessageResponseJson(response, HttpStatus.FORBIDDEN);
//        }
//
//        @Test
//        public void test010() {
//                Admin admin1 = adminList.get(0);
//
//                admin1.setEmail(null);
//                Response response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(admin1));
//                Asserts.assertMessageResponseJson(response, HttpStatus.FORBIDDEN);
//        }
//
//        @Test
//        public void test011() {
//                Admin admin1 = adminList.get(0);
//
//                admin1.setFirstName(null);
//                Response response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(admin1));
//                Asserts.assertMessageResponseJson(response, HttpStatus.FORBIDDEN);
//        }
//
//        @Test
//        public void test012() {
//                Admin admin1 = adminList.get(0);
//
//                admin1.setLastName(null);
//                Response response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(admin1));
//                Asserts.assertMessageResponseJson(response, HttpStatus.FORBIDDEN);
//        }
//
//        @Test
//        public void test013() {
//                Admin admin1 = adminList.get(0);
//
//                Response response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(admin1));
//                admin1.setId(AdminHelper.toAdmin(response.getContent()).getId());
//                Asserts.assertAdminResponseJson(response, HttpStatus.CREATED, admin1);
//
//                admin1.setUsername(null);
//
//                response = Responses.put(url + "/" + admin1.getId(), workspaceHeader, authHeader, RequestModifiers.body(admin1));
//                Asserts.assertMessageResponseJson(response, HttpStatus.FORBIDDEN);
//
//                response = Responses.delete(url + "/" + admin1.getId(), workspaceHeader, authHeader);
//                Asserts.assertMessageResponseJson(response, HttpStatus.GONE);
//        }
//
//        @Test
//        public void test014() {
//                Admin admin1 = adminList.get(0);
//
//                Response response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(admin1));
//                admin1.setId(AdminHelper.toAdmin(response.getContent()).getId());
//                Asserts.assertAdminResponseJson(response, HttpStatus.CREATED, admin1);
//
//                admin1.setPassword(null);
//
//                response = Responses.put(url + "/" + admin1.getId(), workspaceHeader, authHeader, RequestModifiers.body(admin1));
//                Asserts.assertMessageResponseJson(response, HttpStatus.FORBIDDEN);
//
//                response = Responses.delete(url + "/" + admin1.getId(), workspaceHeader, authHeader);
//                Asserts.assertMessageResponseJson(response, HttpStatus.GONE);
//        }
//
//        @Test
//        public void test015() {
//                Admin admin1 = adminList.get(0);
//
//                Response response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(admin1));
//                admin1.setId(AdminHelper.toAdmin(response.getContent()).getId());
//                Asserts.assertAdminResponseJson(response, HttpStatus.CREATED, admin1);
//
//                admin1.setEmail(null);
//
//                response = Responses.put(url + "/" + admin1.getId(), workspaceHeader, authHeader, RequestModifiers.body(admin1));
//                Asserts.assertMessageResponseJson(response, HttpStatus.FORBIDDEN);
//
//                response = Responses.delete(url + "/" + admin1.getId(), workspaceHeader, authHeader);
//                Asserts.assertMessageResponseJson(response, HttpStatus.GONE);
//        }
//
//        @Test
//        public void test016() {
//                Admin admin1 = adminList.get(0);
//
//                Response response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(admin1));
//                admin1.setId(AdminHelper.toAdmin(response.getContent()).getId());
//                Asserts.assertAdminResponseJson(response, HttpStatus.CREATED, admin1);
//
//                admin1.setFirstName(null);
//
//                response = Responses.put(url + "/" + admin1.getId(), workspaceHeader, authHeader, RequestModifiers.body(admin1));
//                Asserts.assertMessageResponseJson(response, HttpStatus.FORBIDDEN);
//
//                response = Responses.delete(url + "/" + admin1.getId(), workspaceHeader, authHeader);
//                Asserts.assertMessageResponseJson(response, HttpStatus.GONE);
//        }
//
//        @Test
//        public void test017() {
//                Admin admin1 = adminList.get(0);
//
//                Response response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(admin1));
//                admin1.setId(AdminHelper.toAdmin(response.getContent()).getId());
//                Asserts.assertAdminResponseJson(response, HttpStatus.CREATED, admin1);
//
//                admin1.setLastName(null);
//
//                response = Responses.put(url + "/" + admin1.getId(), workspaceHeader, authHeader, RequestModifiers.body(admin1));
//                Asserts.assertMessageResponseJson(response, HttpStatus.FORBIDDEN);
//
//                response = Responses.delete(url + "/" + admin1.getId(), workspaceHeader, authHeader);
//                Asserts.assertMessageResponseJson(response, HttpStatus.GONE);
//        }
//
//        @Test
//        public void test0018() {
//                Admin admin1 = adminList.get(0);
//                Admin admin2 = adminList.get(1);
//
//                Response response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(admin1));
//                admin1.setId(AdminHelper.toAdmin(response.getContent()).getId());
//                Asserts.assertAdminResponseJson(response, HttpStatus.CREATED, admin1);
//
//                response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(admin2));
//                admin2.setId(AdminHelper.toAdmin(response.getContent()).getId());
//                Asserts.assertAdminResponseJson(response, HttpStatus.CREATED, admin2);
//
//                response = Responses.get(url, workspaceHeader, authHeader);
//                List<Admin> list = AdminHelper.toList(response.getContent());
//                Asserts.assertListSize(list.size(), 3);
//                Asserts.assertResponseJson(response, HttpStatus.ACCEPTED);
//
//                for (Admin admin : list) {
//                        response = Responses.get(url + "/" + admin.getId(), workspaceHeader, authHeader);
//                        Asserts.assertAdminResponseJson(response, HttpStatus.FOUND, admin);
//                }
//
//                response = Responses.delete(url + "/" + admin1.getId(), workspaceHeader, authHeader);
//                Asserts.assertMessageResponseJson(response, HttpStatus.GONE);
//
//                response = Responses.delete(url + "/" + admin2.getId(), workspaceHeader, authHeader);
//                Asserts.assertMessageResponseJson(response, HttpStatus.GONE);
//        }
//
//        @Test
//        public void test0019() {
//                for (Admin admin : adminList) {
//                        Response response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(admin));
//                        admin.setId(AdminHelper.toAdmin(response.getContent()).getId());
//                        Asserts.assertAdminResponseJson(response, HttpStatus.CREATED, admin);
//                }
//
//                // Default: offset 0, limit 50
//                Response response = Responses.get(url, workspaceHeader, authHeader);
//                Asserts.assertResponseJson(response, HttpStatus.ACCEPTED);
//
//                String offsetLimit = "?offset=1&limit=2";
//                response = Responses.get(url + offsetLimit, workspaceHeader, authHeader);
//                Asserts.assertResponseJson(response, HttpStatus.ACCEPTED);
//                logger.info("Known issue: limit not working properly");
//
//                for (Admin admin : adminList) {
//                        response = Responses.delete(url + "/" + admin.getId(), workspaceHeader, authHeader);
//                        Asserts.assertMessageResponseJson(response, HttpStatus.GONE);
//
//                }
//
//        }
// }
