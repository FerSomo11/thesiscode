package fi.stormcloud.api.suite;

import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.TestContextManager;
import org.springframework.test.context.web.WebAppConfiguration;

import fi.stormcloud.api.ApplicationStarter;

@RunWith(Parameterized.class)
@SpringApplicationConfiguration(classes = ApplicationStarter.class)
@WebAppConfiguration
@IntegrationTest
public abstract class BaseTestSuite {

        private final static Logger logger = LoggerFactory.getLogger(BaseTestSuite.class);

        @Rule
        public TestName testName = new TestName();

        private TestContextManager testContextManager;

        @Before
        public void setUpContext() throws Exception {
                logger.info(testName.getMethodName());

                this.testContextManager = new TestContextManager(getClass());
                this.testContextManager.prepareTestInstance(this);
        }
}
