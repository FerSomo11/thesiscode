package fi.stormcloud.api.suite;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.junit.runners.Parameterized.Parameters;
import org.springframework.http.HttpStatus;

import com.github.restdriver.serverdriver.http.Header;
import com.github.restdriver.serverdriver.http.response.Response;

import fi.stormcloud.api.controller.AdminController;
import fi.stormcloud.api.controller.AuxiliarController;
import fi.stormcloud.api.mgt.Asserts;
import fi.stormcloud.api.mgt.RequestModifiers;
import fi.stormcloud.api.mgt.Responses;
import fi.stormcloud.api.properties.TestProperties;

public class AuthTestSuite extends BaseTestSuite {

        @Rule
        public TestName testName = new TestName();

        private Header workspaceHeader;
        private Header authHeader;
        private String validUrl, invalidUrl;

        public AuthTestSuite(Header authHeader) {
                this.authHeader = authHeader;
        }

        @Parameters(name = "{index}: header({0})")
        public static Collection<Object> data() {
                Header basicAuthHeader = RequestModifiers.basicAuthorizationHeader(TestProperties.INSTANCE.getAdminUsername(), TestProperties.INSTANCE.getAdminPassword());
                Header apiAuthHeader = RequestModifiers.apiAuthorizationHeader(TestProperties.INSTANCE.getAdminApiId(), TestProperties.INSTANCE.getAdminApiSecret());
                return Arrays.asList(new Object[] { basicAuthHeader, apiAuthHeader });
        }

        @Before
        public void setUp() {
                this.workspaceHeader = RequestModifiers.workspaceHeader();
                this.validUrl = AdminController.PATH;
                this.invalidUrl = AuxiliarController.ROOT_PATH;
        }

        @Test
        public void test001() {
                Response response = Responses.get(validUrl);
                Asserts.assertMessageResponseJson(response, HttpStatus.UNAUTHORIZED);
        }

        @Test
        public void test002() {
                Response response = Responses.get(validUrl, authHeader);
                Asserts.assertMessageResponseJson(response, HttpStatus.UNAUTHORIZED);
        }

        @Test
        public void test003() {
                Response response = Responses.get(validUrl, workspaceHeader);
                Asserts.assertMessageResponseJson(response, HttpStatus.UNAUTHORIZED);
        }

        @Test
        public void test004() {
                Response response = Responses.get(invalidUrl, authHeader, workspaceHeader);
                Asserts.assertMessageResponseJson(response, HttpStatus.UNAUTHORIZED);
        }
}
