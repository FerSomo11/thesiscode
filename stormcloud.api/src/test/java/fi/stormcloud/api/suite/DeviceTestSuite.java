//package fi.stormcloud.api.suite;
//
//import java.util.Arrays;
//import java.util.Collection;
//import java.util.List;
//
//import org.apache.commons.lang3.RandomStringUtils;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runners.Parameterized.Parameters;
//import org.springframework.http.HttpStatus;
//
//import com.github.restdriver.serverdriver.http.Header;
//import com.github.restdriver.serverdriver.http.response.Response;
//
//import fi.stormcloud.api.controller.DeviceController;
//import fi.stormcloud.api.helper.DeviceHelper;
//import fi.stormcloud.api.mgt.Asserts;
//import fi.stormcloud.api.mgt.RequestModifiers;
//import fi.stormcloud.api.mgt.Responses;
//import fi.stormcloud.api.properties.TestProperties;
//import fi.stormcloud.core.model.Device;
//
//public class DeviceTestSuite extends BaseTestSuite {
//
//        private List<Device> deviceList;
//        private Header workspaceHeader;
//        private Header authHeader;
//        private String url;
//
//        public DeviceTestSuite(Header authHeader) {
//                this.authHeader = authHeader;
//        }
//
//        @Parameters(name = "{index}: header({0})")
//        public static Collection<Object> data() {
//                Header basicAuthHeader = RequestModifiers.basicAuthorizationHeader(TestProperties.INSTANCE.getAdminUsername(), TestProperties.INSTANCE.getAdminPassword());
//                Header basicApiAuthHeader = RequestModifiers.basicApiAuthorizationHeader(TestProperties.INSTANCE.getAdminApiId(), TestProperties.INSTANCE.getAdminApiSecret());
//                return Arrays.asList(new Object[] { basicAuthHeader, basicApiAuthHeader });
//        }
//
//        @Before
//        public void setUp() {
//                this.deviceList = DeviceHelper.getDeviceList();
//                this.workspaceHeader = RequestModifiers.workspaceHeader();
//                this.url = DeviceController.PATH + "/";
//        }
//
//        @Test
//        public void test001() {
//                Device device1 = deviceList.get(0);
//
//                String random = RandomStringUtils.randomAlphanumeric(15);
//
//                Response response = Responses.get(url + random, workspaceHeader, authHeader);
//                Asserts.assertMessageResponseJson(response, HttpStatus.NOT_FOUND);
//
//                response = Responses.put(url + random, workspaceHeader, authHeader, RequestModifiers.body(device1));
//                Asserts.assertMessageResponseJson(response, HttpStatus.NOT_FOUND);
//
//                response = Responses.delete(url + random, workspaceHeader, authHeader);
//                Asserts.assertMessageResponseJson(response, HttpStatus.NOT_FOUND);
//        }
//
//        @Test
//        public void test002() {
//                Device device1 = deviceList.get(0);
//
//                Response response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(device1));
//                device1.setId(DeviceHelper.toDevice(response.getContent()).getId());
//                Asserts.assertDeviceResponseJson(response, HttpStatus.CREATED, device1);
//
//                response = Responses.get(url + "/" + device1.getId(), workspaceHeader, authHeader);
//                Asserts.assertDeviceResponseJson(response, HttpStatus.FOUND, device1);
//
//                response = Responses.put(url + "/" + device1.getId(), workspaceHeader, authHeader, RequestModifiers.body(device1));
//                Asserts.assertDeviceResponseJson(response, HttpStatus.ACCEPTED, device1);
//
//                response = Responses.delete(url + "/" + device1.getId(), workspaceHeader, authHeader);
//                Asserts.assertMessageResponseJson(response, HttpStatus.GONE);
//        }
//
//        @Test
//        public void test003() {
//                Device device1 = deviceList.get(0);
//
//                Response response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(device1));
//                device1.setId(DeviceHelper.toDevice(response.getContent()).getId());
//                Asserts.assertDeviceResponseJson(response, HttpStatus.CREATED, device1);
//
//                response = Responses.get(url + "/" + device1.getId(), workspaceHeader, authHeader);
//                Asserts.assertDeviceResponseJson(response, HttpStatus.FOUND, device1);
//
//                device1.setTag("DifferentTag");
//                device1.setDescription("DifferentDescription");
//
//                response = Responses.put(url + "/" + device1.getId(), workspaceHeader, authHeader, RequestModifiers.body(device1));
//                Asserts.assertDeviceResponseJson(response, HttpStatus.ACCEPTED, device1);
//
//                response = Responses.get(url + "/" + device1.getId(), workspaceHeader, authHeader);
//                Asserts.assertDeviceResponseJson(response, HttpStatus.FOUND, device1);
//
//                response = Responses.delete(url + "/" + device1.getId(), workspaceHeader, authHeader);
//                Asserts.assertMessageResponseJson(response, HttpStatus.GONE);
//        }
//
//        @Test
//        public void test004() {
//                Device device1 = deviceList.get(0);
//
//                Response response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(device1));
//                device1.setId(DeviceHelper.toDevice(response.getContent()).getId());
//                Asserts.assertDeviceResponseJson(response, HttpStatus.CREATED, device1);
//
//                response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(device1));
//                Asserts.assertMessageResponseJson(response, HttpStatus.UNPROCESSABLE_ENTITY);
//
//                response = Responses.delete(url + "/" + device1.getId(), workspaceHeader, authHeader);
//                Asserts.assertMessageResponseJson(response, HttpStatus.GONE);
//        }
//
//        @Test
//        public void test005() {
//                Device device1 = deviceList.get(0);
//                Device device2 = deviceList.get(1);
//
//                Response response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(device1));
//                device1.setId(DeviceHelper.toDevice(response.getContent()).getId());
//                Asserts.assertDeviceResponseJson(response, HttpStatus.CREATED, device1);
//
//                response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(device2));
//                device2.setId(DeviceHelper.toDevice(response.getContent()).getId());
//                Asserts.assertDeviceResponseJson(response, HttpStatus.CREATED, device2);
//
//                response = Responses.delete(url + "/" + device1.getId(), workspaceHeader, authHeader);
//                Asserts.assertMessageResponseJson(response, HttpStatus.GONE);
//
//                response = Responses.delete(url + "/" + device2.getId(), workspaceHeader, authHeader);
//                Asserts.assertMessageResponseJson(response, HttpStatus.GONE);
//        }
//
//        @Test
//        public void test006() {
//                Device device1 = deviceList.get(0);
//                Device device2 = deviceList.get(1);
//
//                Response response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(device1));
//                device1.setId(DeviceHelper.toDevice(response.getContent()).getId());
//                Asserts.assertDeviceResponseJson(response, HttpStatus.CREATED, device1);
//
//                device1.setSerial(RandomStringUtils.randomAlphanumeric(15));
//
//                response = Responses.put(url + "/" + device1.getId(), workspaceHeader, authHeader, RequestModifiers.body(device1));
//                Asserts.assertDeviceResponseJson(response, HttpStatus.ACCEPTED, device1);
//
//                response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(device2));
//                device2.setId(DeviceHelper.toDevice(response.getContent()).getId());
//                Asserts.assertDeviceResponseJson(response, HttpStatus.CREATED, device2);
//
//                device2.setSerial(device1.getSerial());
//
//                response = Responses.put(url + "/" + device2.getId(), workspaceHeader, authHeader, RequestModifiers.body(device2));
//                Asserts.assertMessageResponseJson(response, HttpStatus.UNPROCESSABLE_ENTITY);
//
//                response = Responses.delete(url + "/" + device1.getId(), workspaceHeader, authHeader);
//                Asserts.assertMessageResponseJson(response, HttpStatus.GONE);
//
//                response = Responses.delete(url + "/" + device2.getId(), workspaceHeader, authHeader);
//                Asserts.assertMessageResponseJson(response, HttpStatus.GONE);
//        }
//
//        @Test
//        public void test007() {
//                Device device1 = deviceList.get(0);
//
//                Response response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(device1));
//                device1.setId(DeviceHelper.toDevice(response.getContent()).getId());
//                Asserts.assertDeviceResponseJson(response, HttpStatus.CREATED, device1);
//
//                device1.setSerial("fakeSerial2");
//                device1.setTag("fakeTag2");
//
//                response = Responses.patch(url + "/" + device1.getId(), workspaceHeader, authHeader, RequestModifiers.body(device1));
//                Asserts.assertDeviceResponseJson(response, HttpStatus.ACCEPTED, device1);
//
//                response = Responses.get(url + "/" + device1.getId(), workspaceHeader, authHeader);
//                Asserts.assertDeviceResponseJson(response, HttpStatus.FOUND, device1);
//
//                response = Responses.delete(url + "/" + device1.getId(), workspaceHeader, authHeader);
//                Asserts.assertMessageResponseJson(response, HttpStatus.GONE);
//        }
//
//        @Test
//        public void test008() {
//                Device device1 = deviceList.get(0);
//
//                device1.setSerial(null);
//                Response response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(device1));
//                Asserts.assertMessageResponseJson(response, HttpStatus.FORBIDDEN);
//        }
//
//        @Test
//        public void test009() {
//                Device device1 = deviceList.get(0);
//
//                device1.setEmail(null);
//                Response response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(device1));
//                Asserts.assertMessageResponseJson(response, HttpStatus.FORBIDDEN);
//        }
//
//        @Test
//        public void test010() {
//                Device device1 = deviceList.get(0);
//
//                Response response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(device1));
//                device1.setId(DeviceHelper.toDevice(response.getContent()).getId());
//                Asserts.assertDeviceResponseJson(response, HttpStatus.CREATED, device1);
//
//                device1.setSerial(null);
//
//                response = Responses.put(url + "/" + device1.getId(), workspaceHeader, authHeader, RequestModifiers.body(device1));
//                Asserts.assertMessageResponseJson(response, HttpStatus.FORBIDDEN);
//
//                response = Responses.delete(url + "/" + device1.getId(), workspaceHeader, authHeader);
//                Asserts.assertMessageResponseJson(response, HttpStatus.GONE);
//        }
//
//        @Test
//        public void test011() {
//                Device device1 = deviceList.get(0);
//
//                Response response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(device1));
//                device1.setId(DeviceHelper.toDevice(response.getContent()).getId());
//                Asserts.assertDeviceResponseJson(response, HttpStatus.CREATED, device1);
//
//                device1.setEmail(null);
//
//                response = Responses.put(url + "/" + device1.getId(), workspaceHeader, authHeader, RequestModifiers.body(device1));
//                Asserts.assertMessageResponseJson(response, HttpStatus.FORBIDDEN);
//
//                response = Responses.delete(url + "/" + device1.getId(), workspaceHeader, authHeader);
//                Asserts.assertMessageResponseJson(response, HttpStatus.GONE);
//        }
//
//        @Test
//        public void test0012() {
//                Device device1 = deviceList.get(0);
//                Device device2 = deviceList.get(1);
//
//                Response response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(device1));
//                device1.setId(DeviceHelper.toDevice(response.getContent()).getId());
//                Asserts.assertDeviceResponseJson(response, HttpStatus.CREATED, device1);
//
//                response = Responses.post(url, workspaceHeader, authHeader, RequestModifiers.body(device2));
//                device2.setId(DeviceHelper.toDevice(response.getContent()).getId());
//                Asserts.assertDeviceResponseJson(response, HttpStatus.CREATED, device2);
//
//                response = Responses.get(url, workspaceHeader, authHeader);
//                List<Device> list = DeviceHelper.toList(response.getContent());
//                Asserts.assertListSize(list.size(), 2);
//                Asserts.assertResponseJson(response, HttpStatus.ACCEPTED);
//
//                for (Device device : list) {
//                        response = Responses.get(url + "/" + device.getId(), workspaceHeader, authHeader);
//                        Asserts.assertDeviceResponseJson(response, HttpStatus.FOUND, device);
//                }
//
//                response = Responses.delete(url + "/" + device1.getId(), workspaceHeader, authHeader);
//                Asserts.assertMessageResponseJson(response, HttpStatus.GONE);
//
//                response = Responses.delete(url + "/" + device2.getId(), workspaceHeader, authHeader);
//                Asserts.assertMessageResponseJson(response, HttpStatus.GONE);
//        }
// }
