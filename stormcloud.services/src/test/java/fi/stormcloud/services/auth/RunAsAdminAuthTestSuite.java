package fi.stormcloud.services.auth;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.junit.Test;

import com.stormpath.sdk.api.ApiKey;

import fi.stormcloud.core.exception.AuthenticationException;
import fi.stormcloud.core.exception.account.AccountException;
import fi.stormcloud.core.exception.group.GroupException;
import fi.stormcloud.core.model.Admin;
import fi.stormcloud.core.model.Device;
import fi.stormcloud.services.helper.AdminHelper;
import fi.stormcloud.services.helper.DeviceHelper;
import fi.stormcloud.services.security.permission.AdminPermissions;
import fi.stormcloud.services.security.realm.BaseRealm.AccessRole;
import fi.stormcloud.services.service.AdminService;
import fi.stormcloud.services.service.DeviceService;

public class RunAsAdminAuthTestSuite extends AuthTestSuite {
        // private final static Logger logger = LoggerFactory.getLogger(RunAsAdminAuthTestSuite.class);

        private AdminService adminService;
        private DeviceService deviceService;

        @Override
        public void setUp() {
                super.setUp();
                adminService = new AdminService();
                deviceService = new DeviceService();
        }

        @Override
        public void tearDown() {
                super.tearDown();
        }

        @Override
        protected UsernamePasswordToken getAuthToken() {
                return new UsernamePasswordToken("admin", "admin");
        }

        @Test
        public void test001() {
                assertTrue(SecurityUtils.getSubject().isAuthenticated());
                assertTrue(SecurityUtils.getSubject().hasRole(AccessRole.SC1.name()));
                assertTrue(SecurityUtils.getSubject().isPermitted(AdminPermissions.ALL));
        }

        @Test
        public void test002() {
                try {
                        Admin admin = adminService.create(application, AdminHelper.build(0));

                        String id = admin.getAdminId();

                        adminService.read(application, id);
                        adminService.list(application);
                        adminService.disable(application, id);
                        adminService.enable(application, id);
                        adminService.update(application, id, AdminHelper.build(1));
                        adminService.partialUpdate(application, id, AdminHelper.build(2));

                        ApiKey apiKey = adminService.createApiKey(application, id);

                        adminService.readApiKey(application, id, apiKey.getId());
                        adminService.disableApiKey(application, id, apiKey.getId());
                        adminService.enableApiKey(application, id, apiKey.getId());
                        adminService.deleteApiKey(application, id, apiKey.getId());

                        adminService.delete(application, id);
                } catch (AccountException | GroupException | AuthenticationException e) {
                        fail(e.getMessage());
                }
        }

        @Test
        public void test003() {
                try {
                        Device device = deviceService.create(application, DeviceHelper.build(0));

                        String id = device.getDeviceId();

                        deviceService.read(application, id);
                        deviceService.list(application);
                        deviceService.disable(application, id);
                        deviceService.enable(application, id);
                        deviceService.update(application, id, DeviceHelper.build(1));
                        deviceService.partialUpdate(application, id, DeviceHelper.build(2));

                        ApiKey apiKey = deviceService.createApiKey(application, id);

                        deviceService.readApiKey(application, id, apiKey.getId());
                        deviceService.disableApiKey(application, id, apiKey.getId());
                        deviceService.enableApiKey(application, id, apiKey.getId());
                        deviceService.deleteApiKey(application, id, apiKey.getId());

                        deviceService.delete(application, id);
                } catch (AccountException | GroupException | AuthenticationException e) {
                        fail(e.getMessage());
                }
        }
}
