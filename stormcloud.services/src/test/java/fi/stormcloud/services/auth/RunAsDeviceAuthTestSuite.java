package fi.stormcloud.services.auth;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.junit.Test;

import fi.stormcloud.core.exception.AuthenticationException;
import fi.stormcloud.core.exception.account.AccountException;
import fi.stormcloud.core.exception.group.GroupException;
import fi.stormcloud.core.exception.group.GroupNotFoundException;
import fi.stormcloud.services.helper.AdminHelper;
import fi.stormcloud.services.helper.DeviceHelper;
import fi.stormcloud.services.security.realm.BaseRealm.AccessRole;
import fi.stormcloud.services.service.AdminService;
import fi.stormcloud.services.service.DeviceService;

public class RunAsDeviceAuthTestSuite extends AuthTestSuite {
        // private final static Logger logger = LoggerFactory.getLogger(RunAsDeviceAuthTestSuite.class);

        private AdminService adminService;
        private DeviceService deviceService;

        @Override
        public void setUp() {
                super.setUp();
                adminService = new AdminService();
                deviceService = new DeviceService();
        }

        @Override
        public void tearDown() {
                super.tearDown();
        }

        @Override
        protected UsernamePasswordToken getAuthToken() {
                return new UsernamePasswordToken("device", "device");
        }

        @Test
        public void test001() {
                assertTrue(SecurityUtils.getSubject().isAuthenticated());
                assertTrue(SecurityUtils.getSubject().hasRole(AccessRole.SC2.name()));
        }

        @Test
        public void test002() {
                try {
                        adminService.create(application, AdminHelper.build(0));
                        deviceService.create(application, DeviceHelper.build(0));
                        fail();
                } catch (AuthenticationException e) {
                        assertTrue(true);
                } catch (AccountException | GroupException e) {
                        fail(e.getMessage());
                }
        }

        @Test
        public void test003() {
                try {
                        adminService.read(application, AdminHelper.build(0).getAdminId());
                        deviceService.read(application, DeviceHelper.build(0).getDeviceId());
                        fail();
                } catch (AuthenticationException e) {
                        assertTrue(true);
                } catch (AccountException e) {
                        fail(e.getMessage());
                }
        }

        @Test
        public void test004() {
                try {
                        adminService.update(application, AdminHelper.build(0).getAdminId(), AdminHelper.build(0));
                        deviceService.update(application, DeviceHelper.build(0).getDeviceId(), DeviceHelper.build(0));
                        fail();
                } catch (AuthenticationException e) {
                        assertTrue(true);
                } catch (AccountException e) {
                        fail(e.getMessage());
                }
        }

        @Test
        public void test005() {
                try {
                        adminService.partialUpdate(application, AdminHelper.build(0).getAdminId(), AdminHelper.build(0));
                        deviceService.partialUpdate(application, DeviceHelper.build(0).getDeviceId(), DeviceHelper.build(0));
                        fail();
                } catch (AuthenticationException e) {
                        assertTrue(true);
                } catch (AccountException e) {
                        fail(e.getMessage());
                }
        }

        @Test
        public void test006() {
                try {
                        adminService.disable(application, AdminHelper.build(0).getAdminId());
                        deviceService.disable(application, DeviceHelper.build(0).getDeviceId());
                        fail();
                } catch (AuthenticationException e) {
                        assertTrue(true);
                } catch (AccountException e) {
                        fail(e.getMessage());
                }
        }

        @Test
        public void test007() {
                try {
                        adminService.enable(application, AdminHelper.build(0).getAdminId());
                        deviceService.enable(application, DeviceHelper.build(0).getDeviceId());
                        fail();
                } catch (AuthenticationException e) {
                        assertTrue(true);
                } catch (AccountException e) {
                        fail(e.getMessage());
                }
        }

        @Test
        public void test008() {
                try {
                        adminService.createApiKey(application, AdminHelper.build(0).getAdminId());
                        deviceService.createApiKey(application, DeviceHelper.build(0).getDeviceId());
                        fail();
                } catch (AuthenticationException e) {
                        assertTrue(true);
                } catch (AccountException e) {
                        fail(e.getMessage());
                }
        }

        @Test
        public void test009() {
                try {
                        adminService.readApiKey(application, AdminHelper.build(0).getAdminId(), AdminHelper.build(0).getAdminId());
                        deviceService.readApiKey(application, DeviceHelper.build(0).getDeviceId(), DeviceHelper.build(0).getDeviceId());
                        fail();
                } catch (AuthenticationException e) {
                        assertTrue(true);
                } catch (AccountException e) {
                        fail(e.getMessage());
                }
        }

        @Test
        public void test010() {
                try {
                        adminService.enableApiKey(application, AdminHelper.build(0).getAdminId(), AdminHelper.build(0).getAdminId());
                        deviceService.enableApiKey(application, DeviceHelper.build(0).getDeviceId(), DeviceHelper.build(0).getDeviceId());
                        fail();
                } catch (AuthenticationException e) {
                        assertTrue(true);
                } catch (AccountException e) {
                        fail(e.getMessage());
                }
        }

        @Test
        public void test011() {
                try {
                        adminService.disableApiKey(application, AdminHelper.build(0).getAdminId(), AdminHelper.build(0).getAdminId());
                        deviceService.disableApiKey(application, DeviceHelper.build(0).getDeviceId(), DeviceHelper.build(0).getDeviceId());
                        fail();
                } catch (AuthenticationException e) {
                        assertTrue(true);
                } catch (AccountException e) {
                        fail(e.getMessage());
                }
        }

        @Test
        public void test012() {
                try {
                        adminService.deleteApiKey(application, AdminHelper.build(0).getAdminId(), AdminHelper.build(0).getAdminId());
                        deviceService.deleteApiKey(application, DeviceHelper.build(0).getDeviceId(), DeviceHelper.build(0).getDeviceId());
                        fail();
                } catch (AuthenticationException e) {
                        assertTrue(true);
                } catch (AccountException e) {
                        fail(e.getMessage());
                }
        }

        @Test
        public void test013() {
                try {
                        adminService.list(application);
                        deviceService.list(application);
                        fail();
                } catch (AuthenticationException e) {
                        assertTrue(true);
                } catch (AccountException | GroupNotFoundException e) {
                        fail(e.getMessage());
                }
        }
}
