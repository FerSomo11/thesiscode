package fi.stormcloud.services.auth;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.util.Factory;
import org.junit.BeforeClass;

import fi.stormcloud.services.BaseTestSuite;

public abstract class AuthTestSuite extends BaseTestSuite {
        // private final static Logger logger = LoggerFactory.getLogger(AuthTestSuite.class);

        @BeforeClass
        public static void setUpContext() {
                Factory<SecurityManager> factory = new IniSecurityManagerFactory("classpath:shiro.ini");
                SecurityManager securityManager = factory.getInstance();
                SecurityUtils.setSecurityManager(securityManager);
        }

        @Override
        public void setUp() {
                super.setUp();
                login(getAuthToken());
        }

        @Override
        public void tearDown() {
                logout();
        }

        private void login(UsernamePasswordToken token) {
                SecurityUtils.getSubject().login(token);
        }

        private void logout() {
                SecurityUtils.getSubject().logout();
        }

        protected abstract UsernamePasswordToken getAuthToken();
}
