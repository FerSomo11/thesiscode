package fi.stormcloud.services.helper;

import org.apache.commons.lang3.builder.CompareToBuilder;

import fi.stormcloud.core.model.Device;

public class DeviceHelper {

        public static Device build(int idx) {
                Device device = new Device();
                device.setSerial("fake" + idx);
                device.setEmail(device.getSerial() + ".fake" + idx + "@fake.fi");
                device.setTag("fake" + idx);
                device.setDescription("fake" + idx);
                device.setEnable(true);
                return device;
        }

        public static int compareDevices(Device a, Device b) {
                CompareToBuilder ctb = new CompareToBuilder();
                ctb.append(a.getDeviceId(), b.getDeviceId());
                ctb.append(a.getSerial(), b.getSerial());
                ctb.append(a.getEmail(), b.getEmail());
                ctb.append(a.getTag(), b.getTag());
                ctb.append(a.getDescription(), b.getDescription());
                ctb.append(a.getEnable(), b.getEnable());
                return ctb.toComparison();
        }
}
