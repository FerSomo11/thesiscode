package fi.stormcloud.services.helper;

import org.apache.commons.lang3.builder.CompareToBuilder;

import fi.stormcloud.core.model.Admin;

public class AdminHelper {

        public static Admin build(int idx) {
                Admin admin = new Admin();
                admin.setUsername("fake" + idx);
                admin.setPassword("fake" + idx);
                admin.setEmail(admin.getUsername() + ".fake" + idx + "@fake.fi");
                admin.setFirstName("fake" + idx);
                admin.setLastName("fake" + idx);
                admin.setEnable(true);
                return admin;
        }

        public static int compareAdmins(Admin a, Admin b) {
                CompareToBuilder ctb = new CompareToBuilder();
                ctb.append(a.getAdminId(), b.getAdminId());
                ctb.append(a.getUsername(), b.getUsername());
                ctb.append(a.getEmail(), b.getEmail());
                ctb.append(a.getFirstName(), b.getFirstName());
                ctb.append(a.getLastName(), b.getLastName());
                ctb.append(a.getEnable(), b.getEnable());
                return ctb.toComparison();
        }

}
