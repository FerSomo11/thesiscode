package fi.stormcloud.services.properties;

import java.io.IOException;
import java.util.Properties;

import org.springframework.core.io.ClassPathResource;

@SuppressWarnings("serial")
public class TestProperties extends Properties {

        public static final TestProperties INSTANCE;
        private static final String RESOURCE_PATH = "fi/stormcloud/services/props/test.properties";

        static {
                try {
                        INSTANCE = (TestProperties) buildCoreTestProperties();
                } catch (Exception e) {
                        throw new ExceptionInInitializerError(e);
                }
        }

        private static Properties buildCoreTestProperties() throws IOException {
                Properties properties = new TestProperties();
                properties.load(new ClassPathResource(RESOURCE_PATH).getInputStream());
                return properties;
        }

        public String getWorkspace() {
                return getProperty("workspace");
        }
}
