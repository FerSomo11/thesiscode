package fi.stormcloud.services;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.stormpath.sdk.application.Application;

import fi.stormcloud.services.dao.ApplicationDao;
import fi.stormcloud.services.properties.TestProperties;

public abstract class BaseTestSuite {
        private final static Logger logger = LoggerFactory.getLogger(BaseTestSuite.class);

        @Rule
        public TestName testName = new TestName();

        protected Application application;

        @Before
        public void setUp() {
                logger.info(testName.getMethodName());
                application = new ApplicationDao().read(TestProperties.INSTANCE.getWorkspace());
        }

        @After
        public void tearDown() {
        }
}
