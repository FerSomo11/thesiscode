//package fi.stormcloud.core.dao;
//
//import static org.junit.Assert.assertTrue;
//import static org.junit.Assert.fail;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.apache.commons.lang3.RandomStringUtils;
//import org.junit.Before;
//import org.junit.Test;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import fi.stormcloud.core.exception.account.AccountException;
//import fi.stormcloud.core.exception.account.AccountNotFoundException;
//import fi.stormcloud.core.exception.group.GroupException;
//import fi.stormcloud.core.exception.group.GroupNotFoundException;
//import fi.stormcloud.core.helper.AdminHelper;
//import fi.stormcloud.core.model.Admin;
//
//public class AdminDaoTests extends BaseDaoTests {
//        private final static Logger logger = LoggerFactory.getLogger(ApplicationDaoTests.class);
//
//        private AdminDao adminDao;
//        private List<Admin> adminList;
//
//        @Before
//        public void setUp() {
//                super.setUp();
//                adminDao = new AdminDao();
//                adminList = AdminHelper.getAdminList();
//        }
//
//        @Test
//        public void test001() {
//                String random = RandomStringUtils.randomAlphanumeric(15);
//                Admin admin = adminList.get(0);
//
//                try {
//                        AdminDao.readById(application, random);
//                        fail();
//                } catch (AccountNotFoundException e1) {
//                        assertTrue(true);
//                }
//
//                try {
//                        AdminDao.update(application, random, admin);
//                        fail();
//                } catch (AccountException e) {
//                        assertTrue(true);
//                }
//
//                try {
//                        AdminDao.delete(application, random);
//                        fail();
//                } catch (AccountException e) {
//                        assertTrue(true);
//                }
//        }
//
//        @Test
//        public void test002() {
//                Admin admin = adminList.get(0);
//
//                try {
//                        Admin a = AdminDao.create(application, admin);
//                        admin.setId(a.getId());
//                        assertTrue(AdminHelper.compareAdmins(a, admin) == 0);
//                } catch (AccountException | GroupException e) {
//                        fail();
//                }
//
//                try {
//                        Admin a = AdminDao.readById(application, admin.getId());
//                        admin.setId(a.getId());
//                        assertTrue(AdminHelper.compareAdmins(a, admin) == 0);
//                } catch (AccountNotFoundException e) {
//                        fail();
//                }
//
//                try {
//                        Admin a = AdminDao.update(application, admin.getId(), admin);
//                        admin.setId(a.getId());
//                        assertTrue(AdminHelper.compareAdmins(a, admin) == 0);
//                } catch (AccountException e) {
//                        fail();
//                }
//
//                try {
//                        boolean b = AdminDao.delete(application, admin.getId());
//                        assertTrue(b);
//                } catch (AccountException e) {
//                        fail();
//                }
//        }
//
//        @Test
//        public void test003() {
//                Admin admin = adminList.get(0);
//
//                try {
//                        Admin a = AdminDao.create(application, admin);
//                        admin.setId(a.getId());
//                        assertTrue(AdminHelper.compareAdmins(a, admin) == 0);
//                } catch (AccountException | GroupException e) {
//                        fail();
//                }
//
//                try {
//                        Admin a = AdminDao.readById(application, admin.getId());
//                        admin.setId(a.getId());
//                        assertTrue(AdminHelper.compareAdmins(a, admin) == 0);
//                } catch (AccountNotFoundException e) {
//                        fail();
//                }
//
//                admin.setFirstName(RandomStringUtils.randomAlphanumeric(15));
//                admin.setLastName(RandomStringUtils.randomAlphanumeric(15));
//
//                try {
//                        Admin a = AdminDao.update(application, admin.getId(), admin);
//                        admin.setId(a.getId());
//                        assertTrue(AdminHelper.compareAdmins(a, admin) == 0);
//                } catch (AccountException e) {
//                        fail();
//                }
//
//                try {
//                        Admin a = AdminDao.readById(application, admin.getId());
//                        admin.setId(a.getId());
//                        assertTrue(AdminHelper.compareAdmins(a, admin) == 0);
//                } catch (AccountNotFoundException e) {
//                        fail();
//                }
//
//                try {
//                        boolean b = AdminDao.delete(application, admin.getId());
//                        assertTrue(b);
//                } catch (AccountException e) {
//                        fail();
//                }
//        }
//
//        @Test
//        public void test004() {
//                Admin admin = adminList.get(0);
//
//                try {
//                        Admin a = AdminDao.create(application, admin);
//                        admin.setId(a.getId());
//                        assertTrue(AdminHelper.compareAdmins(a, admin) == 0);
//                } catch (AccountException | GroupException e) {
//                        fail();
//                }
//
//                try {
//                        AdminDao.create(application, admin);
//                        fail();
//                } catch (AccountException e) {
//                        assertTrue(true);
//                } catch (GroupException e) {
//                        fail();
//                }
//
//                try {
//                        boolean b = AdminDao.delete(application, admin.getId());
//                        assertTrue(b);
//                } catch (AccountException e) {
//                        fail();
//                }
//        }
//
//        @Test
//        public void test005() {
//                Admin admin = adminList.get(0);
//                Admin admin1 = adminList.get(1);
//
//                try {
//                        Admin a = AdminDao.create(application, admin);
//                        admin.setId(a.getId());
//                        assertTrue(AdminHelper.compareAdmins(a, admin) == 0);
//                } catch (AccountException | GroupException e) {
//                        fail();
//                }
//
//                try {
//                        Admin a = AdminDao.create(application, admin1);
//                        admin1.setId(a.getId());
//                        assertTrue(AdminHelper.compareAdmins(a, admin1) == 0);
//                } catch (AccountException | GroupException e) {
//                        fail();
//                }
//
//                try {
//                        boolean b = AdminDao.delete(application, admin.getId());
//                        assertTrue(b);
//                } catch (AccountException e) {
//                        fail();
//                }
//
//                try {
//                        boolean b = AdminDao.delete(application, admin1.getId());
//                        assertTrue(b);
//                } catch (AccountException e) {
//                        fail();
//                }
//        }
//
//        @Test
//        public void test006() {
//                Admin admin = adminList.get(0);
//
//                try {
//                        Admin a = AdminDao.create(application, admin);
//                        admin.setId(a.getId());
//                        assertTrue(AdminHelper.compareAdmins(a, admin) == 0);
//                } catch (AccountException | GroupException e) {
//                        fail();
//                }
//
//                String id = admin.getId();
//                String username = admin.getUsername();
//                admin.setUsername(RandomStringUtils.randomAlphanumeric(15));
//
//                try {
//                        Admin a = AdminDao.update(application, admin.getId(), admin);
//                        admin.setId(a.getId());
//                        assertTrue(AdminHelper.compareAdmins(a, admin) == 0);
//                } catch (AccountException e) {
//                        fail();
//                }
//
//                admin.setUsername(username);
//
//                try {
//                        Admin b = AdminDao.create(application, admin);
//                        admin.setId(b.getId());
//                        assertTrue(AdminHelper.compareAdmins(b, admin) == 0);
//                } catch (AccountException | GroupException e) {
//                        fail();
//                }
//
//                try {
//                        boolean b = AdminDao.delete(application, id);
//                        assertTrue(b);
//                } catch (AccountException e) {
//                        fail();
//                }
//
//                try {
//                        boolean b = AdminDao.delete(application, admin.getId());
//                        assertTrue(b);
//                } catch (AccountException e) {
//                        fail();
//                }
//        }
//
//        @Test
//        public void test007() {
//                Admin admin = adminList.get(0);
//
//                admin.setUsername(null);
//
//                try {
//                        AdminDao.create(application, admin);
//                        fail();
//                } catch (AccountException | GroupException e) {
//                        assertTrue(true);
//                }
//        }
//
//        @Test
//        public void test008() {
//                Admin admin = adminList.get(0);
//
//                admin.setEmail(null);
//
//                try {
//                        AdminDao.create(application, admin);
//                        fail();
//                } catch (AccountException | GroupException e) {
//                        assertTrue(true);
//                }
//        }
//
//        @Test
//        public void test009() {
//                Admin admin = adminList.get(0);
//
//                admin.setFirstName(null);
//
//                try {
//                        AdminDao.create(application, admin);
//                        fail();
//                } catch (AccountException | GroupException e) {
//                        assertTrue(true);
//                }
//        }
//
//        @Test
//        public void test010() {
//                Admin admin = adminList.get(0);
//
//                admin.setLastName(null);
//
//                try {
//                        AdminDao.create(application, admin);
//                        fail();
//                } catch (AccountException | GroupException e) {
//                        assertTrue(true);
//                }
//        }
//
//        @Test
//        public void test011() {
//                Admin admin = adminList.get(0);
//
//                admin.setPassword(null);
//
//                try {
//                        AdminDao.create(application, admin);
//                        fail();
//                } catch (AccountException | GroupException e) {
//                        assertTrue(true);
//                }
//        }
//
//        @Test
//        public void test012() {
//                Admin admin = adminList.get(0);
//
//                try {
//                        Admin a = AdminDao.create(application, admin);
//                        admin.setId(a.getId());
//                        assertTrue(AdminHelper.compareAdmins(a, admin) == 0);
//                } catch (AccountException | GroupException e) {
//                        fail();
//                }
//
//                admin.setUsername(null);
//
//                try {
//                        AdminDao.update(application, admin.getId(), admin);
//                        fail();
//                } catch (AccountException e) {
//                        assertTrue(true);
//                }
//
//                try {
//                        boolean b = AdminDao.delete(application, admin.getId());
//                        assertTrue(b);
//                } catch (AccountException e) {
//                        fail();
//                }
//        }
//
//        public void test013() {
//                Admin admin = adminList.get(0);
//
//                try {
//                        Admin a = AdminDao.create(application, admin);
//                        admin.setId(a.getId());
//                        assertTrue(AdminHelper.compareAdmins(a, admin) == 0);
//                } catch (AccountException | GroupException e) {
//                        fail();
//                }
//
//                admin.setEmail(null);
//
//                try {
//                        AdminDao.update(application, admin.getId(), admin);
//                        fail();
//                } catch (AccountException e) {
//                        assertTrue(true);
//                }
//                try {
//                        boolean b = AdminDao.delete(application, admin.getId());
//                        assertTrue(b);
//                } catch (AccountException e) {
//                        fail();
//                }
//        }
//
//        @Test
//        public void test014() {
//                Admin admin = adminList.get(0);
//
//                try {
//                        Admin a = AdminDao.create(application, admin);
//                        admin.setId(a.getId());
//                        assertTrue(AdminHelper.compareAdmins(a, admin) == 0);
//                } catch (AccountException | GroupException e) {
//                        fail();
//                }
//
//                admin.setFirstName(null);
//
//                try {
//                        AdminDao.update(application, admin.getId(), admin);
//                        fail();
//                } catch (AccountException e) {
//                        assertTrue(true);
//                }
//                try {
//                        boolean b = AdminDao.delete(application, admin.getId());
//                        assertTrue(b);
//                } catch (AccountException e) {
//                        fail();
//                }
//
//        }
//
//        @Test
//        public void test015() {
//                Admin admin = adminList.get(0);
//
//                try {
//                        Admin a = AdminDao.create(application, admin);
//                        admin.setId(a.getId());
//                        assertTrue(AdminHelper.compareAdmins(a, admin) == 0);
//                } catch (AccountException | GroupException e) {
//                        fail();
//                }
//
//                admin.setLastName(null);
//
//                try {
//                        AdminDao.update(application, admin.getId(), admin);
//                        fail();
//                } catch (AccountException e) {
//                        assertTrue(true);
//                }
//                try {
//                        boolean b = AdminDao.delete(application, admin.getId());
//                        assertTrue(b);
//                } catch (AccountException e) {
//                        fail();
//                }
//        }
//
//        @Test
//        public void test016() {
//                Admin admin = adminList.get(0);
//
//                try {
//                        Admin a = AdminDao.create(application, admin);
//                        admin.setId(a.getId());
//                        assertTrue(AdminHelper.compareAdmins(a, admin) == 0);
//                } catch (AccountException | GroupException e) {
//                        fail();
//                }
//
//                admin.setPassword(null);
//
//                try {
//                        AdminDao.update(application, admin.getId(), admin);
//                        fail();
//                } catch (AccountException e) {
//                        assertTrue(true);
//                }
//                try {
//                        boolean b = AdminDao.delete(application, admin.getId());
//                        assertTrue(b);
//                } catch (AccountException e) {
//                        fail();
//                }
//        }
//
//        @Test
//        public void test017() {
//                Admin admin = adminList.get(0);
//                Admin admin1 = adminList.get(1);
//
//                try {
//                        Admin a = AdminDao.create(application, admin);
//                        admin.setId(a.getId());
//                        assertTrue(AdminHelper.compareAdmins(a, admin) == 0);
//                } catch (AccountException | GroupException e) {
//                        fail();
//                }
//
//                try {
//                        Admin a = AdminDao.create(application, admin1);
//                        admin1.setId(a.getId());
//                        assertTrue(AdminHelper.compareAdmins(a, admin1) == 0);
//                } catch (AccountException | GroupException e) {
//                        fail();
//                }
//
//                try {
//                        AdminDao.list(application, 0, 10);
//                } catch (GroupNotFoundException e1) {
//                        fail();
//                }
//
//                try {
//                        boolean b = AdminDao.delete(application, admin.getId());
//                        assertTrue(b);
//                } catch (AccountException e) {
//                        fail();
//                }
//
//                try {
//                        boolean b = AdminDao.delete(application, admin1.getId());
//                        assertTrue(b);
//                } catch (AccountException e) {
//                        fail();
//                }
//        }
//
//        @Test
//        public void test018() {
//
//                for (int i = 0; i < adminList.size(); i++) {
//                        try {
//                                Admin a = AdminDao.create(application, adminList.get(i));
//                                adminList.get(i).setId(a.getId());
//                                assertTrue(AdminHelper.compareAdmins(a, adminList.get(i)) == 0);
//                        } catch (AccountException | GroupException e) {
//                                fail();
//                        }
//                }
//
//                List<Admin> list = new ArrayList<Admin>();
//                try {
//                        list = AdminDao.list(application, 0, 2);
//                } catch (GroupNotFoundException e1) {
//                        fail();
//                }
//                if (list.size() > 2)
//                        logger.info("Known issue: limit not working properly");
//                for (int i = 0; i < adminList.size(); i++) {
//                        try {
//                                boolean b = AdminDao.delete(application, adminList.get(i).getId());
//                                assertTrue(b);
//                        } catch (AccountException e) {
//                                fail();
//                        }
//                }
//
//        }
// }
