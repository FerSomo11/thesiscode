package fi.stormcloud.services.dao;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import fi.stormcloud.services.BaseTestSuite;

public class GroupDaoTestSuite extends BaseTestSuite {
        // private final static Logger logger = LoggerFactory.getLogger(GroupDaoTestSuite.class);

        private GroupDao groupDao;

        @Override
        public void setUp() {
                super.setUp();
                groupDao = new GroupDao();
        }

        @Test
        public void test001() {
                try {
                        assertTrue(groupDao.getAdminGroup(application) != null);
                } catch (Exception e) {
                        fail();
                }
        }

        @Test
        public void test002() {
                try {
                        assertTrue(groupDao.getDeviceGroup(application) != null);
                } catch (Exception e) {
                        fail();
                }
        }
}
