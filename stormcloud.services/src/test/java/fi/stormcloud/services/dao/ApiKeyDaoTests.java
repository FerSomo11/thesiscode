//package fi.stormcloud.core.dao;
//
//import static org.junit.Assert.assertTrue;
//import static org.junit.Assert.fail;
//
//import java.util.List;
//
//import org.apache.commons.lang3.RandomStringUtils;
//import org.junit.Test;
//
//import com.stormpath.sdk.api.ApiKey;
//
//import fi.stormcloud.core.exception.account.AccountException;
//import fi.stormcloud.core.exception.account.AccountNotFoundException;
//import fi.stormcloud.core.exception.apikey.ApiKeyException;
//import fi.stormcloud.core.exception.apikey.ApiKeyNotFoundException;
//import fi.stormcloud.core.exception.group.GroupException;
//import fi.stormcloud.core.helper.AdminHelper;
//import fi.stormcloud.core.model.Admin;
//
//public class ApiKeyDaoTests extends BaseDaoTests {
//        // private final static Logger logger = (Logger) LoggerFactory.getLogger(AccountDaoTests.class);
//
//        private List<Admin> adminList;
//
//        @Override
//        public void setUp() {
//                super.setUp();
//                adminList = AdminHelper.getAdminList();
//        }
//
//
//        @Test
//        public void test001() {
//                Admin admin = adminList.get(0);
//                String apiId = "";
//
//                try {
//                        AdminDao.create(application, admin);
//                } catch (AccountException | GroupException e) {
//                        fail();
//                }
//
//                try {
//                        ApiKey apiKey = ApiKeyDao.create(application, admin.getId());
//                        apiId = apiKey.getId();
//                } catch (ApiKeyException | AccountNotFoundException e) {
//                        fail();
//                }
//
//                try {
//                        boolean b = ApiKeyDao.delete(application, apiId);
//                        assertTrue(b);
//                } catch (ApiKeyException e) {
//                        fail();
//                }
//
//                try {
//                        boolean b = AdminDao.delete(application, admin.getId());
//                        assertTrue(b);
//                } catch (AccountException e) {
//                        fail();
//                }
//        }
//
//        @Test
//        public void test002() {
//                String random = RandomStringUtils.randomAlphanumeric(15);
//                try {
//                        ApiKeyDao.create(application, random);
//                        fail();
//                } catch (AccountException e) {
//                        assertTrue(true);
//                } catch (ApiKeyException e) {
//                        fail();
//                }
//        }
//
//        @Test
//        public void test003() {
//                String random = RandomStringUtils.randomAlphanumeric(15);
//                try {
//                        ApiKeyDao.delete(application, random);
//                        fail();
//                } catch (ApiKeyException e) {
//                        assertTrue(true);
//                }
//        }
//
//        @Test
//        public void test004() {
//                String random = RandomStringUtils.randomAlphanumeric(15);
//                try {
//                        ApiKeyDao.read(application, random);
//                        fail();
//                } catch (ApiKeyNotFoundException e) {
//                        assertTrue(true);
//                }
//        }
// }
