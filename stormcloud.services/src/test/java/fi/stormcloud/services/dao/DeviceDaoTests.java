//package fi.stormcloud.core.dao;
//
//import static org.junit.Assert.assertTrue;
//import static org.junit.Assert.fail;
//
//import java.util.List;
//
//import org.apache.commons.lang3.RandomStringUtils;
//import org.junit.Test;
//
//import fi.stormcloud.core.exception.account.AccountException;
//import fi.stormcloud.core.exception.account.AccountNotFoundException;
//import fi.stormcloud.core.exception.group.GroupException;
//import fi.stormcloud.core.exception.group.GroupNotFoundException;
//import fi.stormcloud.core.helper.DeviceHelper;
//import fi.stormcloud.core.model.Device;
//
//public class DeviceDaoTests extends BaseDaoTests {
//        // private final static Logger logger = LoggerFactory.getLogger(ApplicationDaoTests.class);
//
//        private List<Device> deviceList;
//
//        @Override
//        public void setUp() {
//                super.setUp();
//                deviceList = DeviceHelper.getDeviceList();
//        }
//
//        @Test
//        public void test001() {
//                Device device = deviceList.get(0);
//                String random = RandomStringUtils.randomAlphanumeric(15);
//
//                try {
//                        DeviceDao.readById(application, random);
//                        fail();
//                } catch (AccountNotFoundException e1) {
//                        assertTrue(true);
//                }
//
//                try {
//                        DeviceDao.update(application, random, device);
//                        fail();
//                } catch (AccountException e) {
//                        assertTrue(true);
//                }
//
//                try {
//                        DeviceDao.delete(application, random);
//                        fail();
//                } catch (AccountException e) {
//                        assertTrue(true);
//                }
//        }
//
//        @Test
//        public void test002() {
//                Device device = deviceList.get(0);
//
//                try {
//                        Device a = DeviceDao.create(application, device);
//                        device.setId(a.getId());
//                        assertTrue(DeviceHelper.compareDevices(a, device) == 0);
//                } catch (AccountException | GroupException e) {
//                        fail();
//                }
//
//                try {
//                        Device a = DeviceDao.readById(application, device.getId());
//                        device.setId(a.getId());
//                        assertTrue(DeviceHelper.compareDevices(a, device) == 0);
//                } catch (AccountNotFoundException e) {
//                        fail();
//                }
//
//                try {
//                        Device a = DeviceDao.update(application, device.getId(), device);
//                        device.setId(a.getId());
//                        assertTrue(DeviceHelper.compareDevices(a, device) == 0);
//                } catch (AccountException e) {
//                        fail();
//                }
//
//                try {
//                        boolean b = DeviceDao.delete(application, device.getId());
//                        assertTrue(b);
//                } catch (AccountException e) {
//                        fail();
//                }
//        }
//
//        @Test
//        public void test003() {
//                Device device = deviceList.get(0);
//
//                try {
//                        Device a = DeviceDao.create(application, device);
//                        device.setId(a.getId());
//                        assertTrue(DeviceHelper.compareDevices(a, device) == 0);
//                } catch (AccountException | GroupException e) {
//                        fail();
//                }
//
//                try {
//                        Device a = DeviceDao.readById(application, device.getId());
//                        device.setId(a.getId());
//                        assertTrue(DeviceHelper.compareDevices(a, device) == 0);
//                } catch (AccountNotFoundException e) {
//                        fail();
//                }
//
//                device.setTag(RandomStringUtils.randomAlphanumeric(15));
//                device.setDescription(RandomStringUtils.randomAlphanumeric(15));
//
//                try {
//                        Device a = DeviceDao.update(application, device.getId(), device);
//                        device.setId(a.getId());
//                        assertTrue(DeviceHelper.compareDevices(a, device) == 0);
//                } catch (AccountException e) {
//                        fail();
//                }
//
//                try {
//                        Device a = DeviceDao.readById(application, device.getId());
//                        device.setId(a.getId());
//                        assertTrue(DeviceHelper.compareDevices(a, device) == 0);
//                } catch (AccountNotFoundException e) {
//                        fail();
//                }
//
//                try {
//                        boolean b = DeviceDao.delete(application, device.getId());
//                        assertTrue(b);
//                } catch (AccountException e) {
//                        fail();
//                }
//        }
//
//        @Test
//        public void test004() {
//                Device device = deviceList.get(0);
//
//                try {
//                        Device a = DeviceDao.create(application, device);
//                        device.setId(a.getId());
//                        assertTrue(DeviceHelper.compareDevices(a, device) == 0);
//                } catch (AccountException | GroupException e) {
//                        fail();
//                }
//
//                try {
//                        DeviceDao.create(application, device);
//                        fail();
//                } catch (AccountException e) {
//                        assertTrue(true);
//                } catch (GroupException e) {
//                        fail();
//                }
//
//                try {
//                        boolean b = DeviceDao.delete(application, device.getId());
//                        assertTrue(b);
//                } catch (AccountException e) {
//                        fail();
//                }
//        }
//
//        @Test
//        public void test005() {
//                Device device = deviceList.get(0);
//                Device device1 = deviceList.get(1);
//
//                try {
//                        Device a = DeviceDao.create(application, device);
//                        device.setId(a.getId());
//                        assertTrue(DeviceHelper.compareDevices(a, device) == 0);
//                } catch (AccountException | GroupException e) {
//                        fail();
//                }
//
//                try {
//                        Device a = DeviceDao.create(application, device1);
//                        device1.setId(a.getId());
//                        assertTrue(DeviceHelper.compareDevices(a, device1) == 0);
//                } catch (AccountException | GroupException e) {
//                        fail();
//                }
//
//                try {
//                        boolean b = DeviceDao.delete(application, device.getId());
//                        assertTrue(b);
//                } catch (AccountException e) {
//                        fail();
//                }
//
//                try {
//                        boolean b = DeviceDao.delete(application, device1.getId());
//                        assertTrue(b);
//                } catch (AccountException e) {
//                        fail();
//                }
//        }
//
//        @Test
//        public void test006() {
//                Device device = deviceList.get(0);
//
//                try {
//                        Device a = DeviceDao.create(application, device);
//                        device.setId(a.getId());
//                        assertTrue(DeviceHelper.compareDevices(a, device) == 0);
//                } catch (AccountException | GroupException e) {
//                        fail();
//                }
//
//                String id = device.getId();
//                String serial = device.getSerial();
//                device.setSerial(RandomStringUtils.randomAlphanumeric(15));
//
//                try {
//                        Device a = DeviceDao.update(application, device.getId(), device);
//                        device.setId(a.getId());
//                        assertTrue(DeviceHelper.compareDevices(a, device) == 0);
//                } catch (AccountException e) {
//                        fail();
//                }
//
//                device.setSerial(serial);
//
//                try {
//                        Device a = DeviceDao.create(application, device);
//                        device.setId(a.getId());
//                        assertTrue(DeviceHelper.compareDevices(a, device) == 0);
//                } catch (AccountException | GroupException e) {
//                        fail();
//                }
//
//                try {
//                        boolean b = DeviceDao.delete(application, id);
//                        assertTrue(b);
//                } catch (AccountException e) {
//                        fail();
//                }
//
//                try {
//                        boolean b = DeviceDao.delete(application, device.getId());
//                        assertTrue(b);
//                } catch (AccountException e) {
//                        fail();
//                }
//        }
//
//        @Test
//        public void test007() {
//                Device device = deviceList.get(0);
//                device.setSerial(null);
//
//                try {
//                        DeviceDao.create(application, device);
//                        fail();
//                } catch (AccountException | GroupException e) {
//                        assertTrue(true);
//                }
//        }
//
//        @Test
//        public void test008() {
//                Device device = deviceList.get(0);
//                device.setEmail(null);
//
//                try {
//                        DeviceDao.create(application, device);
//                        fail();
//                } catch (AccountException | GroupException e) {
//                        assertTrue(true);
//                }
//        }
//
//        @Test
//        public void test009() {
//                Device device = deviceList.get(0);
//                try {
//                        Device a = DeviceDao.create(application, device);
//                        device.setId(a.getId());
//                        assertTrue(DeviceHelper.compareDevices(a, device) == 0);
//                } catch (AccountException | GroupException e) {
//                        fail();
//                }
//
//                device.setSerial(null);
//
//                try {
//                        DeviceDao.update(application, device.getId(), device);
//                        fail();
//                } catch (AccountException e) {
//                        assertTrue(true);
//                }
//
//                try {
//                        boolean b = DeviceDao.delete(application, device.getId());
//                        assertTrue(b);
//                } catch (AccountException e) {
//                        fail();
//                }
//        }
//
//        @Test
//        public void test010() {
//                Device device = deviceList.get(0);
//                try {
//                        Device a = DeviceDao.create(application, device);
//                        device.setId(a.getId());
//                        assertTrue(DeviceHelper.compareDevices(a, device) == 0);
//                } catch (AccountException | GroupException e) {
//                        fail();
//                }
//
//                device.setEmail(null);
//
//                try {
//                        DeviceDao.update(application, device.getId(), device);
//                        fail();
//                } catch (AccountException e) {
//                        assertTrue(true);
//                }
//
//                try {
//                        boolean b = DeviceDao.delete(application, device.getId());
//                        assertTrue(b);
//                } catch (AccountException e) {
//                        fail();
//                }
//        }
//
//        @Test
//        public void test011() {
//                Device device = deviceList.get(0);
//                Device device1 = deviceList.get(1);
//                try {
//                        Device a = DeviceDao.create(application, device);
//                        device.setId(a.getId());
//                        assertTrue(DeviceHelper.compareDevices(a, device) == 0);
//                } catch (AccountException | GroupException e) {
//                        fail();
//                }
//
//                try {
//                        Device a = DeviceDao.create(application, device1);
//                        device1.setId(a.getId());
//                        assertTrue(DeviceHelper.compareDevices(a, device1) == 0);
//                } catch (AccountException | GroupException e) {
//                        fail();
//                }
//
//                try {
//                        DeviceDao.list(application);
//                } catch (GroupNotFoundException e1) {
//                        fail();
//                }
//
//                try {
//                        boolean b = DeviceDao.delete(application, device.getId());
//                        assertTrue(b);
//                } catch (AccountException e) {
//                        fail();
//                }
//
//                try {
//                        boolean b = DeviceDao.delete(application, device1.getId());
//                        assertTrue(b);
//                } catch (AccountException e) {
//                        fail();
//                }
//        }
// }
