package fi.stormcloud.services.dao;

import static org.junit.Assert.assertTrue;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;

import fi.stormcloud.services.BaseTestSuite;

public class ApplicationDaoTestSuite extends BaseTestSuite {
        // private final static Logger logger = LoggerFactory.getLogger(ApplicationDaoTestSuite.class);

        private ApplicationDao applicationDao;

        @Override
        public void setUp() {
                super.setUp();
                applicationDao = new ApplicationDao();
        }

        @Test
        public void test001() {
                assertTrue(applicationDao.read(RandomStringUtils.randomAlphanumeric(200)) == null);
        }
}
