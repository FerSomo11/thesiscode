package fi.stormcloud.services.util;

import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang3.time.DateFormatUtils;

public class DateUtil {

        public static Date parseISO8601(String source) throws ParseException {
                return DateFormatUtils.ISO_DATETIME_TIME_ZONE_FORMAT.parse(source);
        }

        public static String formatISO8601(Date date) {
                return DateFormatUtils.ISO_DATETIME_TIME_ZONE_FORMAT.format(date);
        }
}
