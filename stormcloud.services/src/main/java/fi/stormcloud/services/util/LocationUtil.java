package fi.stormcloud.services.util;

import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fi.stormcloud.core.model.Location;

public class LocationUtil {

        public static String formatISO6709(Double latitude, Double longitude) {
                return new IsoPosition(latitude, longitude).toString();
        }

        public static IsoPosition parseISO8601(String source) throws ParseException {
                return IsoPosition.parse(source);
        }
        
        public static Location toLocation(String source) throws ParseException{
                IsoPosition isoPosition= parseISO8601(source);
                return new Location(isoPosition.getLatitude(),isoPosition.getLongitude());
        }

        private static class IsoPosition {
                private final Double latitude, longitude;

                public IsoPosition(Double latitude, Double longitude) {
                        this.latitude = latitude;
                        this.longitude = longitude;
                }

                public Double getLongitude() {
                        return longitude;
                }

                public Double getLatitude() {
                        return latitude;
                }

                public String toString() {
                        StringBuilder buf = new StringBuilder();

                        double ln = getLatitude();
                        String lat = String.format("%+f", getLatitude());
                        if (ln < 10 && ln > -10)
                                lat = lat.replaceFirst("([+-])", "$10");
                        buf.append(lat);

                        ln = getLongitude();
                        String lon = String.format("%+f", ln);
                        if (ln < 100 && ln > -100)
                                lon = lon.replaceFirst("([+-])", "$10");
                        if (ln < 10 && ln > -10)
                                lon = lon.replaceFirst("([+-])", "$10");
                        buf.append(lon);

                        return buf.append('/').toString();
                }

                private static double toDegrees(Integer d1, Integer m1, Integer s1, Double f1) {
                        double d = d1 != null ? Math.abs(d1) : 0;
                        double m = m1 != null ? m1 : 0.0;
                        double s = s1 != null ? s1 : 0.0;
                        if (s1 != null && f1 != null)
                                s += f1;
                        else if (m1 != null && f1 != null)
                                m += f1;
                        else if (f1 != null)
                                d += f1;
                        double ret = d + (m / 60) + (s / 3600);
                        return d1 != null && d1 < 0 ? -ret : ret;
                }

                public static IsoPosition parse(String source) throws ParseException {
                        Pattern pattern = Pattern.compile("(([+-]\\d{2})(\\d{2})?(\\d{2})?(\\.\\d+)?)(([+-]\\d{3})(\\d{2})?(\\d{2})?(\\.\\d+)?)?");
                        Matcher matcher = pattern.matcher(source);
                        Integer latd = null, latm = null, lats = null, lond = null, lonm = null, lons = null;
                        Double latf = null, lonf = null;
                        if (matcher.find()) {
                                if (matcher.group(1) != null) {
                                        String sdeg = matcher.group(2);
                                        String smin = matcher.group(3);
                                        String ssec = matcher.group(4);
                                        String sfrac = matcher.group(5);
                                        latd = sdeg != null ? Integer.valueOf(sdeg.charAt(0) == '+' ? sdeg.substring(1) : sdeg) : null;
                                        latm = smin != null ? Integer.valueOf(smin) : null;
                                        lats = ssec != null ? Integer.valueOf(ssec) : null;
                                        latf = sfrac != null ? Double.valueOf(sfrac) : null;
                                }
                                if (matcher.group(6) != null) {
                                        String sdeg = matcher.group(7);
                                        String smin = matcher.group(8);
                                        String ssec = matcher.group(9);
                                        String sfrac = matcher.group(10);
                                        lond = sdeg != null ? Integer.valueOf(sdeg.charAt(0) == '+' ? sdeg.substring(1) : sdeg) : null;
                                        lonm = smin != null ? Integer.valueOf(smin) : null;
                                        lons = ssec != null ? Integer.valueOf(ssec) : null;
                                        lonf = sfrac != null ? Double.valueOf(sfrac) : null;
                                }
                                return new IsoPosition(toDegrees(latd, latm, lats, latf), toDegrees(lond, lonm, lons, lonf));
                        }
                        throw new ParseException("Unparseable location: \"" + source + "\"", 0);
                }
        }
}
