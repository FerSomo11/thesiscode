package fi.stormcloud.services.util;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

import com.stormpath.sdk.account.Account;
import com.stormpath.sdk.account.AccountStatus;

import fi.stormcloud.core.model.Device;

public class DeviceUtil {
        public static final String GROUP_NAME = "Devices";

        public static final boolean isDevice(Account account) {
                return account != null && account.isMemberOfGroup(GROUP_NAME);
        }

        public static boolean isValid(Device device) {
                return device != null && StringUtils.isNoneBlank(device.getSerial(), device.getEmail());
        }

        public static Account toAccount(Device device, Account account) {
                if (device != null && account != null) {
                        account.setMiddleName(device.getDeviceId());
                        account.setUsername(device.getSerial());
                        account.setPassword(RandomStringUtils.randomAlphanumeric(90));
                        account.setEmail(account.getUsername() + "." + device.getEmail());
                        account.setGivenName(device.getSerial());
                        account.setSurname(device.getSerial());
                        account.setStatus((device.getEnable()) ? AccountStatus.ENABLED : AccountStatus.DISABLED);

                        account.getCustomData().put("tag", device.getTag());
                        account.getCustomData().put("description", device.getDescription());
                }
                return account;
        }

        public static Device fromAccount(Account account) {
                if (account != null) {
                        Device device = new Device();
                        device.setDeviceId(account.getMiddleName());
                        device.setSerial(account.getUsername());
                        device.setEmail(StringUtils.removeStart(account.getEmail(), account.getUsername() + "."));
                        device.setEnable(AccountStatus.ENABLED.equals(account.getStatus()));

                        device.setTag((String) account.getCustomData().get("tag"));
                        device.setDescription((String) account.getCustomData().get("description"));
                        device.setCreatedBy((String) account.getCustomData().get("createdBy"));
                        device.setModifiedBy((String) account.getCustomData().get("modifiedBy"));
                        device.setCreatedAt(DateUtil.formatISO8601(account.getCustomData().getCreatedAt()));
                        device.setModifiedAt(DateUtil.formatISO8601(account.getCustomData().getModifiedAt()));

                        return device;
                }
                return null;
        }
}
