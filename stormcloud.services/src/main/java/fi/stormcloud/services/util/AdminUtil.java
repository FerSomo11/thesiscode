package fi.stormcloud.services.util;

import org.apache.commons.lang3.StringUtils;

import com.stormpath.sdk.account.Account;
import com.stormpath.sdk.account.AccountStatus;

import fi.stormcloud.core.model.Admin;

public class AdminUtil {
        public static final String GROUP_NAME = "Admins";

        public static final boolean isAdmin(Account account) {
                return account != null && account.isMemberOfGroup(GROUP_NAME);
        }

        public static boolean isValid(Admin admin) {
                return admin != null && StringUtils.isNoneBlank(admin.getUsername(), admin.getPassword(), admin.getEmail(), admin.getFirstName(), admin.getLastName());
        }

        public static Account toAccount(Admin admin, Account account) {
                if (admin != null && account != null) {
                        account.setMiddleName(admin.getAdminId());
                        account.setUsername(admin.getUsername());
                        account.setPassword(admin.getPassword());
                        account.setEmail(account.getUsername() + "." + admin.getEmail());
                        account.setGivenName(admin.getFirstName());
                        account.setSurname(admin.getLastName());
                        account.setStatus((admin.getEnable()) ? AccountStatus.ENABLED : AccountStatus.DISABLED);
                }
                return account;
        }

        public static Admin fromAccount(Account account) {
                if (account != null) {
                        Admin admin = new Admin();
                        admin.setAdminId(account.getMiddleName());
                        admin.setUsername(account.getUsername());
                        admin.setPassword(null);
                        admin.setEmail(StringUtils.removeStart(account.getEmail(), account.getUsername() + "."));
                        admin.setFirstName(account.getGivenName());
                        admin.setLastName(account.getSurname());
                        admin.setEnable(AccountStatus.ENABLED.equals(account.getStatus()));
                        admin.setCreatedBy((String) account.getCustomData().get("createdBy"));
                        admin.setModifiedBy((String) account.getCustomData().get("modifiedBy"));
                        admin.setCreatedAt(DateUtil.formatISO8601(account.getCustomData().getCreatedAt()));
                        admin.setModifiedAt(DateUtil.formatISO8601(account.getCustomData().getModifiedAt()));
                        return admin;
                }
                return null;
        }
}
