package fi.stormcloud.services.service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.stormpath.sdk.account.Account;
import com.stormpath.sdk.account.AccountList;
import com.stormpath.sdk.api.ApiKey;
import com.stormpath.sdk.application.Application;
import com.stormpath.sdk.group.Group;

import fi.stormcloud.core.exception.AuthenticationException;
import fi.stormcloud.core.exception.account.AccountAlreadyExistsException;
import fi.stormcloud.core.exception.account.AccountException;
import fi.stormcloud.core.exception.account.AccountMissingFieldsException;
import fi.stormcloud.core.exception.account.AccountNotFoundException;
import fi.stormcloud.core.exception.group.GroupException;
import fi.stormcloud.core.exception.group.GroupNotFoundException;
import fi.stormcloud.core.model.Admin;
import fi.stormcloud.services.client.StormpathClient;
import fi.stormcloud.services.dao.AdminDao;
import fi.stormcloud.services.dao.ApiKeyDao;
import fi.stormcloud.services.dao.GroupDao;
import fi.stormcloud.services.security.permission.AdminPermissions;
import fi.stormcloud.services.util.AdminUtil;

public class AdminService {
        private static final Logger logger = LoggerFactory.getLogger(AdminService.class);

        private AdminDao adminDao;
        private GroupDao groupDao;
        private ApiKeyDao apiKeyDao;

        private EmailService emailService;

        public AdminService() {
                adminDao = new AdminDao();
                groupDao = new GroupDao();
                apiKeyDao = new ApiKeyDao();

                emailService = new EmailService();
        }

        public Admin read(Application application, String id) throws AccountNotFoundException, AuthenticationException {
                if (!SecurityUtils.getSubject().isPermitted(AdminPermissions.READ)) {
                        throw new AuthenticationException();
                }

                Account account = adminDao.readById(application, id);
                return AdminUtil.fromAccount(account);
        }
        
        public Admin readByUsername(Application application, String username) throws AccountNotFoundException, AuthenticationException{
        	if(!SecurityUtils.getSubject().isPermitted(AdminPermissions.READ)){
        		throw new AuthenticationException();
        	}
        	
        	Account account = adminDao.readByUsername(application, username);
        	return AdminUtil.fromAccount(account);
        }

        public Admin create(Application application, Admin admin) throws AccountException, GroupException, AuthenticationException {
                if (!SecurityUtils.getSubject().isPermitted(AdminPermissions.CREATE)) {
                        throw new AuthenticationException();
                }

                if (!AdminUtil.isValid(admin)) {
                        throw new AccountMissingFieldsException();
                }

                if (adminDao.existsByUsername(application, admin.getUsername())) {
                        throw new AccountAlreadyExistsException();
                }

                admin.setAdminId(UUID.randomUUID().toString());
                while (adminDao.existsById(application, admin.getAdminId())) {
                        admin.setAdminId(UUID.randomUUID().toString());
                }

                try {
                        Account account = StormpathClient.INSTANCE.instantiate(Account.class);
                        account = AdminUtil.toAccount(admin, account);
                        account = adminDao.create(application, account);
                        return AdminUtil.fromAccount(account);
                } catch (Exception e) {
                        logger.error(e.getMessage());
                        throw new AccountException();
                }
        }

        public boolean delete(Application application, String id) throws AccountException, AuthenticationException {
                if (!SecurityUtils.getSubject().isPermitted(AdminPermissions.DELETE)) {
                        throw new AuthenticationException();
                }

                if (!adminDao.existsById(application, id)) {
                        throw new AccountNotFoundException();
                }
                return adminDao.delete(application, id);
        }

        public Admin update(Application application, String id, Admin admin) throws AccountException, AuthenticationException {
                if (!SecurityUtils.getSubject().isPermitted(AdminPermissions.UPDATE)) {
                        throw new AuthenticationException();
                }

                if (!AdminUtil.isValid(admin)) {
                        throw new AccountMissingFieldsException();
                }

                if (!adminDao.existsById(application, id)) {
                        throw new AccountNotFoundException();
                }

                if (adminDao.existsByUsername(application, admin.getUsername())) {
                        Account account = adminDao.readByUsername(application, admin.getUsername());
                        Admin dbAdmin = AdminUtil.fromAccount(account);
                        if (!StringUtils.equals(dbAdmin.getAdminId(), id)) {
                                throw new AccountAlreadyExistsException();
                        }
                }

                try {
                        Account account = adminDao.readById(application, id);
                        Admin dbo = AdminUtil.fromAccount(account);
                        admin.setAdminId(dbo.getAdminId());
                        account = AdminUtil.toAccount(admin, account);
                        account = adminDao.save(account);
                        return AdminUtil.fromAccount(account);
                } catch (Exception e) {
                        logger.error(e.getMessage());
                        throw new AccountException();
                }
        }

        public Admin enable(Application application, String id) throws AccountException, AuthenticationException {
                if (!SecurityUtils.getSubject().isPermitted(AdminPermissions.UPDATE)) {
                        throw new AuthenticationException();
                }

                if (!adminDao.existsById(application, id)) {
                        throw new AccountNotFoundException();
                }

                try {
                        Account account = adminDao.enable(application, id);
                        return AdminUtil.fromAccount(account);
                } catch (Exception e) {
                        logger.error(e.getMessage());
                        throw new AccountException();
                }
        }

        public Admin disable(Application application, String id) throws AccountException, AuthenticationException {
                if (!SecurityUtils.getSubject().isPermitted(AdminPermissions.UPDATE)) {
                        throw new AuthenticationException();
                }

                if (!adminDao.existsById(application, id)) {
                        throw new AccountNotFoundException();
                }

                try {
                        Account account = adminDao.disable(application, id);
                        return AdminUtil.fromAccount(account);
                } catch (Exception e) {
                        logger.error(e.getMessage());
                        throw new AccountException();
                }
        }

        public Admin partialUpdate(Application application, String id, Admin patch) throws AccountException, AuthenticationException {
                if (!SecurityUtils.getSubject().isPermitted(AdminPermissions.UPDATE)) {
                        throw new AuthenticationException();
                }

                if (!adminDao.existsById(application, id)) {
                        throw new AccountNotFoundException();
                }

                Account account = adminDao.readById(application, id);
                Admin admin = AdminUtil.fromAccount(account);

                if (StringUtils.isNotBlank(patch.getUsername())) {
                        admin.setUsername(patch.getUsername());
                }
                if (StringUtils.isNotBlank(patch.getPassword())) {
                        admin.setPassword(patch.getPassword());
                }
                if (StringUtils.isNotBlank(patch.getEmail())) {
                        admin.setEmail(patch.getEmail());
                }
                if (StringUtils.isNotBlank(patch.getFirstName())) {
                        admin.setFirstName(patch.getFirstName());
                }
                if (StringUtils.isNotBlank(patch.getLastName())) {
                        admin.setLastName(patch.getLastName());
                }

                return update(application, id, admin);
        }

        public List<Admin> list(Application application) throws GroupNotFoundException, AccountException, AuthenticationException {
                if (!SecurityUtils.getSubject().isPermitted(AdminPermissions.READ)) {
                        throw new AuthenticationException();
                }

                try {
                        Group group = groupDao.getAdminGroup(application);
                        AccountList accountList = group.getAccounts();
                        List<Admin> admins = new ArrayList<Admin>();
                        for (Account account : accountList) {
                                admins.add(AdminUtil.fromAccount(account));
                        }
                        return admins;
                } catch (Exception e) {
                        logger.error(e.getMessage());
                        throw new AccountException();
                }
        }

        public ApiKey createApiKey(Application application, String id) throws AccountException, AuthenticationException {
                if (!SecurityUtils.getSubject().isPermitted(AdminPermissions.UPDATE)) {
                        throw new AuthenticationException();
                }

                Account account = adminDao.readById(application, id);

                try {
                        ApiKey apiKey = apiKeyDao.create(account);
                        emailService.sendNewApiKeyEmail(apiKey);
                        return apiKey;
                } catch (Exception e) {
                        logger.error(e.getMessage());
                        throw new AccountException();
                }
        }

        public ApiKey readApiKey(Application application, String id, String apiId) throws AccountException, AuthenticationException {
                if (!SecurityUtils.getSubject().isPermitted(AdminPermissions.UPDATE)) {
                        throw new AuthenticationException();
                }

                Account account = adminDao.readById(application, id);

                try {
                        return apiKeyDao.read(account, apiId);
                } catch (Exception e) {
                        logger.error(e.getMessage());
                        throw new AccountException();
                }
        }

        public boolean deleteApiKey(Application application, String id, String apiId) throws AccountException, AuthenticationException {
                if (!SecurityUtils.getSubject().isPermitted(AdminPermissions.UPDATE)) {
                        throw new AuthenticationException();
                }

                Account account = adminDao.readById(application, id);

                try {
                        return apiKeyDao.delete(account, apiId);
                } catch (Exception e) {
                        logger.error(e.getMessage());
                        throw new AccountException();
                }
        }

        public ApiKey enableApiKey(Application application, String id, String apiId) throws AccountException, AuthenticationException {
                if (!SecurityUtils.getSubject().isPermitted(AdminPermissions.UPDATE)) {
                        throw new AuthenticationException();
                }

                Account account = adminDao.readById(application, id);

                try {
                        return apiKeyDao.enable(account, apiId);
                } catch (Exception e) {
                        logger.error(e.getMessage());
                        throw new AccountException();
                }
        }

        public ApiKey disableApiKey(Application application, String id, String apiId) throws AccountException, AuthenticationException {
                if (!SecurityUtils.getSubject().isPermitted(AdminPermissions.UPDATE)) {
                        throw new AuthenticationException();
                }

                Account account = adminDao.readById(application, id);

                try {
                        return apiKeyDao.disable(account, apiId);
                } catch (Exception e) {
                        logger.error(e.getMessage());
                        throw new AccountException();
                }
        }
}
