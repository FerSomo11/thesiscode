package fi.stormcloud.services.service;

import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.sendgrid.SendGrid.Email;
import com.sendgrid.SendGridException;
import com.stormpath.sdk.account.Account;
import com.stormpath.sdk.api.ApiKey;

import fi.stormcloud.core.model.Admin;
import fi.stormcloud.core.model.Device;
import fi.stormcloud.services.client.SendGridClient;
import fi.stormcloud.services.properties.EmailProperties;
import fi.stormcloud.services.util.AdminUtil;
import fi.stormcloud.services.util.DeviceUtil;

public class EmailService {
        private final static Logger logger = LoggerFactory.getLogger(EmailService.class);

        private static final String API_KEY_REQUEST_EMAIL_RESOURCE_PATH = "fi/stormcloud/services/templates/apiKeyRequestEmail.txt";

        public final void sendNewApiKeyEmail(ApiKey apiKey) {
                if (apiKey != null) {
                        Resource resource = new ClassPathResource(API_KEY_REQUEST_EMAIL_RESOURCE_PATH);

                        try {
                                String text = IOUtils.toString(resource.getInputStream());
                                text = StringUtils.replace(text, "<%apiId%>", apiKey.getId());
                                text = StringUtils.replace(text, "<%apiSecret%>", apiKey.getSecret());

                                sendEmail(getAddress(apiKey), text);

                        } catch (IOException e) {
                                logger.error(e.getMessage());
                        }
                }
        }

        private final String getAddress(ApiKey apiKey) {
                if (apiKey != null) {
                        Account account = apiKey.getAccount();
                        if (AdminUtil.isAdmin(account)) {
                                Admin admin = AdminUtil.fromAccount(account);
                                return admin.getEmail();
                        } else if (DeviceUtil.isDevice(account)) {
                                Device device = DeviceUtil.fromAccount(account);
                                return device.getEmail();
                        }
                }
                return StringUtils.EMPTY;
        }

        private final void sendEmail(String address, String text) {
                if (StringUtils.isNoneBlank(address, text)) {
                        Email email = new Email();

                        email.addTo(address);
                        email.setFrom(EmailProperties.INSTANCE.getFrom());
                        email.setFromName(EmailProperties.INSTANCE.getFromName());
                        email.setReplyTo(EmailProperties.INSTANCE.getReplyTo());
                        email.setSubject(EmailProperties.INSTANCE.getSubjectApiKeyRequest());

                        email.setText(text);

                        try {
                                SendGridClient.INSTANCE.send(email);
                        } catch (SendGridException e) {
                                logger.error(e.getMessage());
                        }
                }
        }
}
