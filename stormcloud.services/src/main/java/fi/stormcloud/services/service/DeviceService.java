package fi.stormcloud.services.service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.stormpath.sdk.account.Account;
import com.stormpath.sdk.account.AccountList;
import com.stormpath.sdk.api.ApiKey;
import com.stormpath.sdk.application.Application;
import com.stormpath.sdk.group.Group;

import fi.stormcloud.core.exception.AuthenticationException;
import fi.stormcloud.core.exception.account.AccountAlreadyExistsException;
import fi.stormcloud.core.exception.account.AccountException;
import fi.stormcloud.core.exception.account.AccountMissingFieldsException;
import fi.stormcloud.core.exception.account.AccountNotFoundException;
import fi.stormcloud.core.exception.group.GroupException;
import fi.stormcloud.core.model.Device;
import fi.stormcloud.services.client.StormpathClient;
import fi.stormcloud.services.dao.ApiKeyDao;
import fi.stormcloud.services.dao.DeviceDao;
import fi.stormcloud.services.dao.GroupDao;
import fi.stormcloud.services.security.permission.DevicePermissions;
import fi.stormcloud.services.util.DeviceUtil;

public class DeviceService {
        private static final Logger logger = LoggerFactory.getLogger(DeviceService.class);

        private DeviceDao deviceDao;
        private GroupDao groupDao;
        private ApiKeyDao apiKeyDao;

        private EmailService emailService;

        public DeviceService() {
                deviceDao = new DeviceDao();
                groupDao = new GroupDao();
                apiKeyDao = new ApiKeyDao();

                emailService = new EmailService();
        }

        public Device read(Application application, String id) throws AccountNotFoundException, AuthenticationException {
                if (!SecurityUtils.getSubject().isPermitted(DevicePermissions.READ)) {
                        throw new AuthenticationException();
                }

                Account account = deviceDao.readById(application, id);
                return DeviceUtil.fromAccount(account);
        }

        public Device create(Application application, Device device) throws AccountException, GroupException, AuthenticationException {
                if (!SecurityUtils.getSubject().isPermitted(DevicePermissions.CREATE)) {
                        throw new AuthenticationException();
                }

                if (!DeviceUtil.isValid(device)) {
                        throw new AccountMissingFieldsException();
                }

                if (deviceDao.existsBySerial(application, device.getSerial())) {
                        throw new AccountAlreadyExistsException();
                }

                device.setDeviceId(UUID.randomUUID().toString());
                while (deviceDao.existsById(application, device.getDeviceId())) {
                        device.setDeviceId(UUID.randomUUID().toString());
                }

                try {
                        Account account = StormpathClient.INSTANCE.instantiate(Account.class);
                        account = DeviceUtil.toAccount(device, account);
                        account = deviceDao.create(application, account);
                        return DeviceUtil.fromAccount(account);
                } catch (Exception e) {
                        logger.error(e.getMessage());
                        throw new AccountException();
                }
        }

        public boolean delete(Application application, String id) throws AccountException, AuthenticationException {
                if (!SecurityUtils.getSubject().isPermitted(DevicePermissions.DELETE)) {
                        throw new AuthenticationException();
                }

                if (!deviceDao.existsById(application, id)) {
                        throw new AccountNotFoundException();
                }
                return deviceDao.delete(application, id);
        }

        public Device update(Application application, String id, Device device) throws AccountException, AuthenticationException {
                if (!SecurityUtils.getSubject().isPermitted(DevicePermissions.UPDATE)) {
                        throw new AuthenticationException();
                }

                if (!DeviceUtil.isValid(device)) {
                        throw new AccountMissingFieldsException();
                }

                if (!deviceDao.existsById(application, id)) {
                        throw new AccountNotFoundException();
                }

                if (deviceDao.existsBySerial(application, device.getSerial())) {
                        Account account = deviceDao.readBySerial(application, device.getSerial());
                        Device dbDevices = DeviceUtil.fromAccount(account);
                        if (!StringUtils.equals(dbDevices.getDeviceId(), id)) {
                                throw new AccountAlreadyExistsException();
                        }
                }

                try {
                        Account account = deviceDao.readById(application, id);
                        Device dbo = DeviceUtil.fromAccount(account);
                        device.setDeviceId(dbo.getDeviceId());
                        account = DeviceUtil.toAccount(device, account);
                        account = deviceDao.save(account);
                        return DeviceUtil.fromAccount(account);
                } catch (Exception e) {
                        logger.error(e.getMessage());
                        throw new AccountException();
                }
        }

        public Device enable(Application application, String id) throws AccountException, AuthenticationException {
                if (!SecurityUtils.getSubject().isPermitted(DevicePermissions.UPDATE)) {
                        throw new AuthenticationException();
                }

                if (!deviceDao.existsById(application, id)) {
                        throw new AccountNotFoundException();
                }

                Account account = deviceDao.enable(application, id);
                return DeviceUtil.fromAccount(account);
        }

        public Device disable(Application application, String id) throws AccountException, AuthenticationException {
                if (!SecurityUtils.getSubject().isPermitted(DevicePermissions.UPDATE)) {
                        throw new AuthenticationException();
                }

                if (!deviceDao.existsById(application, id)) {
                        throw new AccountNotFoundException();
                }

                Account account = deviceDao.disable(application, id);
                return DeviceUtil.fromAccount(account);
        }

        public Device partialUpdate(Application application, String id, Device patch) throws AccountException, AuthenticationException {
                if (!SecurityUtils.getSubject().isPermitted(DevicePermissions.UPDATE)) {
                        throw new AuthenticationException();
                }

                if (!deviceDao.existsById(application, id)) {
                        throw new AccountNotFoundException();
                }

                Account account = deviceDao.readById(application, id);
                Device device = DeviceUtil.fromAccount(account);

                if (StringUtils.isNotBlank(patch.getSerial())) {
                        device.setSerial(patch.getSerial());
                }

                if (StringUtils.isNotBlank(patch.getEmail())) {
                        device.setEmail(patch.getEmail());
                }

                if (patch.getTag() != null) {
                        device.setTag(StringUtils.trimToEmpty(patch.getTag()));
                }

                if (patch.getDescription() != null) {
                        device.setDescription(StringUtils.trimToEmpty(patch.getDescription()));
                }

                return update(application, id, DeviceUtil.fromAccount(account));
        }

        public List<Device> list(Application application) throws AuthenticationException, AccountException {
                if (!SecurityUtils.getSubject().isPermitted(DevicePermissions.READ)) {
                        throw new AuthenticationException();
                }

                try {
                        Group group = groupDao.getDeviceGroup(application);
                        AccountList accountList = group.getAccounts();
                        List<Device> devices = new ArrayList<Device>();
                        for (Account account : accountList) {
                                devices.add(DeviceUtil.fromAccount(account));
                        }
                        return devices;
                } catch (Exception e) {
                        logger.error(e.getMessage());
                        throw new AccountException();
                }
        }

        public ApiKey createApiKey(Application application, String id) throws AccountException, AuthenticationException {
                if (!SecurityUtils.getSubject().isPermitted(DevicePermissions.UPDATE)) {
                        throw new AuthenticationException();
                }

                Account account = deviceDao.readById(application, id);

                try {
                        ApiKey apiKey = apiKeyDao.create(account);
                        emailService.sendNewApiKeyEmail(apiKey);
                        return apiKey;
                } catch (Exception e) {
                        logger.error(e.getMessage());
                        throw new AccountException();
                }
        }

        public ApiKey readApiKey(Application application, String id, String apiId) throws AccountException, AuthenticationException {
                if (!SecurityUtils.getSubject().isPermitted(DevicePermissions.UPDATE)) {
                        throw new AuthenticationException();
                }

                Account account = deviceDao.readById(application, id);

                try {
                        return apiKeyDao.read(account, apiId);
                } catch (Exception e) {
                        logger.error(e.getMessage());
                        throw new AccountException();
                }
        }

        public boolean deleteApiKey(Application application, String id, String apiId) throws AccountException, AuthenticationException {
                if (!SecurityUtils.getSubject().isPermitted(DevicePermissions.UPDATE)) {
                        throw new AuthenticationException();
                }

                Account account = deviceDao.readById(application, id);

                try {
                        return apiKeyDao.delete(account, apiId);
                } catch (Exception e) {
                        logger.error(e.getMessage());
                        throw new AccountException();
                }
        }

        public ApiKey enableApiKey(Application application, String id, String apiId) throws AccountException, AuthenticationException {
                if (!SecurityUtils.getSubject().isPermitted(DevicePermissions.UPDATE)) {
                        throw new AuthenticationException();
                }

                Account account = deviceDao.readById(application, id);

                try {
                        return apiKeyDao.enable(account, apiId);
                } catch (Exception e) {
                        logger.error(e.getMessage());
                        throw new AccountException();
                }
        }

        public ApiKey disableApiKey(Application application, String id, String apiId) throws AccountException, AuthenticationException {
                if (!SecurityUtils.getSubject().isPermitted(DevicePermissions.UPDATE)) {
                        throw new AuthenticationException();
                }

                Account account = deviceDao.readById(application, id);

                try {
                        return apiKeyDao.disable(account, apiId);
                } catch (Exception e) {
                        logger.error(e.getMessage());
                        throw new AccountException();
                }
        }
}
