package fi.stormcloud.services.service;

import io.orchestrate.client.Result;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import fi.stormcloud.core.exception.AuthenticationException;
import fi.stormcloud.core.exception.data.DataCreatedFailedException;
import fi.stormcloud.core.exception.data.DataMissingFieldsException;
import fi.stormcloud.core.exception.data.DataNotFoundException;
import fi.stormcloud.core.model.Data;
import fi.stormcloud.core.model.Location;
import fi.stormcloud.services.dao.DataDao;
import fi.stormcloud.services.security.permission.DataPermissions;
import fi.stormcloud.services.util.DateUtil;
import fi.stormcloud.services.util.LocationUtil;

public class DataService {
        // private static final Logger logger = LoggerFactory.getLogger(RawDataService.class);

        private DataDao dataDao;

        public DataService() {
                this.dataDao = new DataDao();
        }

        public String push(String source) throws AuthenticationException, IOException, DataNotFoundException, DataCreatedFailedException {
//                if (!SecurityUtils.getSubject().isPermitted(DataPermissions.PUSH)) {
//                        throw new AuthenticationException();
//                }
                JSONObject jsonObject = new JSONObject(source);
                Data data = new Data();
                data.setRawData(jsonObject.toString());
                try {
                        if (jsonObject.has("type"))
                                data.setType(jsonObject.getString("type"));
                        if (jsonObject.has("timestamp"))
                                data.setTimestamp(DateUtil.parseISO8601(jsonObject.getString("timestamp")).getTime());
                        if (jsonObject.has("location"))
                                data.setLocation(LocationUtil.toLocation(jsonObject.getString("location")));
                } catch (JSONException | ParseException e) {
                        throw new DataCreatedFailedException();
                }
                // dataDao.create(data).getKey(); if we need the String Key
                return dataDao.create(data).getValue().getRawData();
        }

        public List<String> pullByTimestamp(String start, String end) throws AuthenticationException, DataMissingFieldsException, DataNotFoundException {
                if (!SecurityUtils.getSubject().isPermitted(DataPermissions.PULL)) {
                        throw new AuthenticationException();
                }
                return pullByTimestamp((String) SecurityUtils.getSubject().getPrincipal(), start, end);
        }

        public List<String> pullByTimestamp(String createdBy, String start, String end) throws DataMissingFieldsException, DataNotFoundException {
                if (StringUtils.isBlank(start) || StringUtils.isBlank(end)) {
                        throw new DataMissingFieldsException();
                }

                try {
                        List<Result<Data>> resultList = dataDao.listByTimeRange(createdBy, DateUtil.parseISO8601(start).getTime(), DateUtil.parseISO8601(end).getTime());

                        if (CollectionUtils.isNotEmpty(resultList)) {
                                List<String> list = new ArrayList<String>();
                                for (Result<Data> data : resultList) {
                                        list.add(data.getKvObject().getValue().getRawData());
                                }
                                return list;
                        }
                        return new ArrayList<String>();
                } catch (ParseException e) {
                        throw new DataNotFoundException();
                }
        }

        public List<String> pullByLocation(String location, String distance) throws DataNotFoundException, AuthenticationException, DataMissingFieldsException {
                if (!SecurityUtils.getSubject().isPermitted(DataPermissions.PULL)) {
                        throw new AuthenticationException();
                }
                if (StringUtils.isBlank(location) || !StringUtils.isNumeric(distance)) {
                        throw new DataMissingFieldsException();
                }
                Location point = null;
                try {
                        point = LocationUtil.toLocation(location);
                } catch (ParseException e) {
                        throw new DataNotFoundException();
                }

                List<Result<Data>> resultList = dataDao.listByLocation(point, distance);

                if (CollectionUtils.isNotEmpty(resultList)) {
                        List<String> list = new ArrayList<String>();
                        for (Result<Data> data : resultList) {
                                list.add(data.getKvObject().getValue().getRawData());
                        }
                        return list;
                }
                return new ArrayList<String>();

        }
        public List<Data> pullAll() throws DataMissingFieldsException, DataNotFoundException {

                List<Result<Data>> resultList = dataDao.listAll();

                if (CollectionUtils.isNotEmpty(resultList)) {
                        List<Data> list = new ArrayList<Data>();
                        for (Result<Data> data : resultList) {
                                list.add(data.getKvObject().getValue());
                        }
                        return list;
                }
                return new ArrayList<Data>();
        }
}
