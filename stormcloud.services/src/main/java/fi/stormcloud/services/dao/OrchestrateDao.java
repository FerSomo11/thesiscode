package fi.stormcloud.services.dao;

import java.io.IOException;

import fi.stormcloud.services.client.OrchestrateClient;
import io.orchestrate.client.Client;
import io.orchestrate.client.EventMetadata;
import io.orchestrate.client.KvMetadata;
import io.orchestrate.client.KvObject;
import io.orchestrate.client.SearchResults;

public abstract class OrchestrateDao<T> {

        private Client client;

        private String collectionName;
        private Class<T> clazz;

        public OrchestrateDao(String collectionName, Class<T> clazz) {
                this.collectionName = collectionName;
                this.clazz = clazz;
                this.client = OrchestrateClient.INSTANCE;
        }

        protected KvObject<T> fetch(String key) {
                return client.kv(collectionName, key).get(clazz).get();
        }
        
        protected EventMetadata createEvent(T obj,String key,String type){
                return client.event(collectionName, key).type(type).create(obj).get();
        }

        protected KvMetadata post(T obj) throws IOException {
                return client.postValue(collectionName, obj).get();
        }

        protected KvMetadata put(String key, T obj) {
                return client.kv(collectionName, key).put(obj).get();
        }

        protected Boolean delete(String key) {
                return client.kv(collectionName, key).delete().get();
        }

         protected SearchResults<T> search(String luceneQuery) {
         return client.searchCollection(collectionName).get(clazz, luceneQuery).get();
         }

}