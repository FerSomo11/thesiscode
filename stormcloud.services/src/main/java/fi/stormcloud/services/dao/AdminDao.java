package fi.stormcloud.services.dao;

import org.apache.shiro.SecurityUtils;

import com.stormpath.sdk.account.Account;
import com.stormpath.sdk.application.Application;

import fi.stormcloud.core.exception.account.AccountException;
import fi.stormcloud.core.exception.account.AccountNotFoundException;
import fi.stormcloud.core.exception.group.GroupException;
import fi.stormcloud.services.util.AdminUtil;

public class AdminDao extends AccountDao {
        // private final Logger logger = LoggerFactory.getLogger(AdminDao.class);

        private GroupDao groupDao;

        public AdminDao() {
                groupDao = new GroupDao();
        }

        public Account create(Application application, Account account) throws AccountException, GroupException {
                account = application.createAccount(account);
                account.addGroup(groupDao.getAdminGroup(application));
                account.getCustomData().put("createdBy", SecurityUtils.getSubject().getPrincipal());
                account.getCustomData().put("modifiedBy", SecurityUtils.getSubject().getPrincipal());
                account.getCustomData().save();
                return account;
        }

        @Override
        public Account readById(Application application, String id) throws AccountNotFoundException {
                Account account = super.readById(application, id);
                if (AdminUtil.isAdmin(account)) {
                        return account;
                }
                throw new AccountNotFoundException();
        }

        public Account readByUsername(Application application, String username) throws AccountNotFoundException {
                Account account = readByUsernameOrSerial(application, username);
                if (AdminUtil.isAdmin(account)) {
                        return account;
                }
                throw new AccountNotFoundException();
        }

        public boolean existsByUsername(Application application, String username) {
                try {
                        return readByUsername(application, username) != null;
                } catch (AccountNotFoundException e) {
                        return false;
                }
        }

        public boolean existsById(Application application, String id) {
                try {
                        return readById(application, id) != null;
                } catch (AccountNotFoundException e) {
                        return false;
                }
        }
}
