package fi.stormcloud.services.dao;

import org.apache.shiro.SecurityUtils;

import com.stormpath.sdk.account.Account;
import com.stormpath.sdk.application.Application;

import fi.stormcloud.core.exception.account.AccountException;
import fi.stormcloud.core.exception.account.AccountNotFoundException;
import fi.stormcloud.core.exception.group.GroupException;
import fi.stormcloud.services.util.DeviceUtil;

public class DeviceDao extends AccountDao {

        // private final Logger logger = LoggerFactory.getLogger(DeviceDao.class);

        private GroupDao groupDao;

        public DeviceDao() {
                groupDao = new GroupDao();
        }

        public Account create(Application application, Account account) throws AccountException, GroupException {
                account = application.createAccount(account);
                account.addGroup(groupDao.getDeviceGroup(application));
                account.getCustomData().put("createdBy", SecurityUtils.getSubject().getPrincipal());
                account.getCustomData().put("modifiedBy", SecurityUtils.getSubject().getPrincipal());
                account.getCustomData().save();
                return account;
        }

        @Override
        public Account readById(Application application, String id) throws AccountNotFoundException {
                Account account = super.readById(application, id);
                if (DeviceUtil.isDevice(account)) {
                        return account;
                }
                throw new AccountNotFoundException();
        }

        public Account readBySerial(Application application, String serial) throws AccountNotFoundException {
                Account account = readByUsernameOrSerial(application, serial);
                if (DeviceUtil.isDevice(account)) {
                        return account;
                }
                throw new AccountNotFoundException();
        }

        public boolean existsBySerial(Application application, String serial) {
                try {
                        return readBySerial(application, serial) != null;
                } catch (AccountNotFoundException e) {
                        return false;
                }
        }

        public boolean existsById(Application application, String id) {
                try {
                        return readById(application, id) != null;
                } catch (AccountNotFoundException e) {
                        return false;
                }
        }
}
