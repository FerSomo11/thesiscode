package fi.stormcloud.services.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.stormpath.sdk.account.Account;
import com.stormpath.sdk.api.ApiKey;
import com.stormpath.sdk.api.ApiKeyList;
import com.stormpath.sdk.api.ApiKeyStatus;

import fi.stormcloud.core.exception.apikey.ApiKeyException;
import fi.stormcloud.core.exception.apikey.ApiKeyNotFoundException;

public class ApiKeyDao {
        private final static Logger logger = LoggerFactory.getLogger(ApiKeyDao.class);

        public ApiKey create(Account account) throws ApiKeyException {
                try {
                        return account.createApiKey();
                } catch (Exception e) {
                        logger.error(e.getMessage());
                        throw new ApiKeyException();
                }
        }

        public ApiKey read(Account account, String apiId) throws ApiKeyNotFoundException {
                try {
                        ApiKeyList apiKeyList = account.getApiKeys();
                        for (ApiKey apiKey : apiKeyList) {
                                if (apiKey.getId().equals(apiId)) {
                                        return apiKey;
                                }
                        }
                } catch (Exception e) {
                        logger.error(e.getMessage());
                }
                throw new ApiKeyNotFoundException();
        }

        public ApiKey save(Account account, String apiId) throws ApiKeyException {
                ApiKey apiKey = read(account, apiId);
                try {
                        apiKey.save();
                        return apiKey;
                } catch (Exception e) {
                        logger.error(e.getMessage());
                        throw new ApiKeyException();
                }
        }

        public boolean delete(Account account, String apiId) throws ApiKeyException {
                ApiKey apiKey = read(account, apiId);
                try {
                        apiKey.delete();
                        return true;
                } catch (Exception e) {
                        logger.error(e.getMessage());
                        throw new ApiKeyException();
                }
        }

        public ApiKey enable(Account account, String apiId) throws ApiKeyException {
                ApiKey apiKey = read(account, apiId);
                try {
                        apiKey.setStatus(ApiKeyStatus.ENABLED);
                        return apiKey;
                } catch (Exception e) {
                        logger.error(e.getMessage());
                        throw new ApiKeyException();
                }
        }

        public ApiKey disable(Account account, String apiId) throws ApiKeyException {
                ApiKey apiKey = read(account, apiId);
                try {
                        apiKey.setStatus(ApiKeyStatus.DISABLED);
                        save(account, apiId);
                        return apiKey;
                } catch (Exception e) {
                        logger.error(e.getMessage());
                        throw new ApiKeyException();
                }
        }
}
