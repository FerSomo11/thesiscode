package fi.stormcloud.services.dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.orchestrate.client.EventMetadata;
import io.orchestrate.client.KvObject;
import io.orchestrate.client.Result;
import io.orchestrate.client.SearchResults;
import fi.stormcloud.core.exception.data.DataCreatedFailedException;
import fi.stormcloud.core.exception.data.DataDeleteFailedException;
import fi.stormcloud.core.exception.data.DataNotFoundException;
import fi.stormcloud.core.model.Data;
import fi.stormcloud.core.model.Location;
import fi.stormcloud.services.util.DateUtil;

public class DataDao extends OrchestrateDao<Data> {
        private static final Logger logger = LoggerFactory.getLogger(DataDao.class);

        private static final String COLLECTION_NAME = "DataCollection";

        public DataDao() {
                super(COLLECTION_NAME, Data.class);
        }

        public KvObject<Data> read(String key) throws DataNotFoundException {
                if (StringUtils.isNotBlank(key)) {
                        try {
                                KvObject<Data> kv = fetch(key);
                                if (kv != null) {
                                        return kv;
                                }
                        } catch (Exception e) {
                                logger.error(e.getMessage());
                        }
                }
                throw new DataNotFoundException();

        }

        public KvObject<Data> create(Data data) throws IOException, DataNotFoundException, DataCreatedFailedException {
                try {
                        data.setCreatedBy(SecurityUtils.getSubject().getPrincipal().toString());
                        data.setCreatedAt(DateUtil.formatISO8601(new Date()));
                        String generatedKey = post(data).getKey();
                        EventMetadata event = createEvent(data, generatedKey, data.getType());
                        data.setTimestamp(event.getTimestamp());
                        return fetch(generatedKey);
                } catch (Exception e) {
                        logger.error(e.getMessage());
                        throw new DataCreatedFailedException();
                }

        }

        public Boolean remove(String key) throws DataNotFoundException, DataDeleteFailedException {
                read(key);
                try {
                        return delete(key);

                } catch (Exception e) {
                        logger.error(e.getMessage());
                        throw new DataDeleteFailedException();
                }
        }

        public List<Result<Data>> listByTimeRange(String createdBy, Long start, Long end) {
                List<Result<Data>> list = new ArrayList<Result<Data>>();
                String luceneQuery = "value.createdBy: " + "`" + createdBy + "`" + " AND " + "value.timestamp: [" + start + " TO " + end + "]";
                SearchResults<Data> results = search(luceneQuery);
                for (Result<Data> rawDataResult : results) {
                        list.add(rawDataResult);
                }
                return list;
        }

        public List<Result<Data>> listByLocation(Location point, String distance) {
                List<Result<Data>> list = new ArrayList<Result<Data>>();
                String luceneQuery = "value.location:NEAR:{lat:" + point.getLatitude() + " lon:" + point.getLongitude() + " dist:" + distance + "m}";
                SearchResults<Data> results = search(luceneQuery);
                for (Result<Data> rawDataResult : results) {
                        list.add(rawDataResult);
                }
                return list;
        }
        
        public List<Result<Data>> listAll() {
                List<Result<Data>> list = new ArrayList<Result<Data>>();
                String luceneQuery = "*";
                SearchResults<Data> results = search(luceneQuery);
                for (Result<Data> rawDataResult : results) {
                        list.add(rawDataResult);
                }
                return list;
        }

}