package fi.stormcloud.services.dao;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IteratorUtils;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.stormpath.sdk.account.Account;
import com.stormpath.sdk.account.AccountCriteria;
import com.stormpath.sdk.account.AccountList;
import com.stormpath.sdk.account.AccountStatus;
import com.stormpath.sdk.account.Accounts;
import com.stormpath.sdk.api.ApiKey;
import com.stormpath.sdk.application.Application;

import fi.stormcloud.core.exception.account.AccountException;
import fi.stormcloud.core.exception.account.AccountNotFoundException;
import fi.stormcloud.core.exception.apikey.ApiKeyException;
import fi.stormcloud.core.exception.group.GroupException;

public abstract class AccountDao {
        private static final Logger logger = LoggerFactory.getLogger(AccountDao.class);

        public Account create(Application application, Account account) throws AccountException, GroupException {
                try {
                        account = application.createAccount(account);
                        return account;
                } catch (Exception e) {
                        logger.error(e.getMessage());
                        throw new AccountException();
                }
        }

        public Account save(Account account) throws AccountException {
                try {
                        account.save();
                        account.getCustomData().put("modifiedBy", SecurityUtils.getSubject().getPrincipal());
                        account.getCustomData().save();
                        return account;
                } catch (Exception e) {
                        logger.error(e.getMessage());
                        throw new AccountException();
                }
        }

        protected Account readByUsernameOrSerial(Application application, String usernameOrSerial) throws AccountNotFoundException {
                return readWithCriteria(application, Accounts.where(Accounts.username().eqIgnoreCase(usernameOrSerial)));
        }

        public Account readById(Application application, String id) throws AccountNotFoundException {
                return readWithCriteria(application, Accounts.where(Accounts.middleName().eqIgnoreCase(id)));
        }

        private Account readWithCriteria(Application application, AccountCriteria criteria) throws AccountNotFoundException {
                if (application != null) {
                        AccountList accountList = application.getAccounts(criteria);
                        List<Account> accounts = IteratorUtils.toList(accountList.iterator());
                        if (CollectionUtils.isNotEmpty(accounts)) {
                                return accounts.get(0);
                        }
                }
                throw new AccountNotFoundException();
        }

        public boolean delete(Application application, String id) throws AccountException {
                Account account = readById(application, id);
                try {
                        account.delete();
                        return true;
                } catch (Exception e) {
                        logger.error(e.getMessage());
                        throw new AccountException();
                }
        }

        public Account enable(Application application, String id) throws AccountException {
                return enable(application, id, AccountStatus.ENABLED);
        }

        public Account disable(Application application, String id) throws AccountException {
                return enable(application, id, AccountStatus.DISABLED);
        }

        private Account enable(Application application, String id, AccountStatus accountStatus) throws AccountException {
                Account account = readById(application, id);
                try {
                        account.setStatus(accountStatus);
                        save(account);
                        return account;
                } catch (Exception e) {
                        logger.error(e.getMessage());
                        throw new AccountException();
                }
        }

        public ApiKey createApiKey(Application application, String id) throws AccountNotFoundException, ApiKeyException {
                Account account = readById(application, id);
                try {
                        return account.createApiKey();
                } catch (Exception e) {
                        logger.error(e.getMessage());
                        throw new ApiKeyException();
                }
        }
}
