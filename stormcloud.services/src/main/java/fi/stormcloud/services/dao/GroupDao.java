package fi.stormcloud.services.dao;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IteratorUtils;

import com.stormpath.sdk.application.Application;
import com.stormpath.sdk.group.Group;
import com.stormpath.sdk.group.GroupList;
import com.stormpath.sdk.group.Groups;

import fi.stormcloud.core.exception.group.GroupNotFoundException;
import fi.stormcloud.services.util.AdminUtil;
import fi.stormcloud.services.util.DeviceUtil;

public class GroupDao {
        // private static final Logger logger = LoggerFactory.getLogger(GroupDao.class);

        private Group getGroup(Application application, String name) throws GroupNotFoundException {
                if (application != null) {
                        GroupList groupList = application.getGroups(Groups.where(Groups.name().eqIgnoreCase(name)));
                        List<Group> groups = IteratorUtils.toList(groupList.iterator());
                        if (CollectionUtils.isNotEmpty(groups)) {
                                return groups.get(0);
                        }
                }
                throw new GroupNotFoundException();
        }

        public Group getAdminGroup(Application application) throws GroupNotFoundException {
                return getGroup(application, AdminUtil.GROUP_NAME);
        }

        public Group getDeviceGroup(Application application) throws GroupNotFoundException {
                return getGroup(application, DeviceUtil.GROUP_NAME);
        }
}
