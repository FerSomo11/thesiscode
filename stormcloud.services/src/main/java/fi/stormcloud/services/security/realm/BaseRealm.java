package fi.stormcloud.services.security.realm;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.stormpath.sdk.account.Account;
import com.stormpath.sdk.application.Application;

import fi.stormcloud.core.exception.account.AccountNotFoundException;
import fi.stormcloud.services.dao.AdminDao;
import fi.stormcloud.services.dao.ApplicationDao;
import fi.stormcloud.services.security.authc.WorkspaceAuthenticationInfo;
import fi.stormcloud.services.security.authc.WorkspaceAuthenticationToken;
import fi.stormcloud.services.security.authc.WorkspacePrincipalCollection;
import fi.stormcloud.services.security.permission.CustomRolePermissionResolver;
import fi.stormcloud.services.util.AdminUtil;
import fi.stormcloud.services.util.DeviceUtil;

public abstract class BaseRealm extends AuthorizingRealm {

        private final static Logger logger = LoggerFactory.getLogger(BaseRealm.class);

        public enum AccessRole {
                SC0, // SUPER_ADMIN
                SC1, // WORKSPACE_ADMIN
                SC2 // DEVICE
        }

        public enum LoginEntryPoint {
                UI,
                REST_BASIC,
                REST_API
        }

        private AdminDao adminDao;
        private ApplicationDao applicationDao;

        public BaseRealm() {
                setRolePermissionResolver(new CustomRolePermissionResolver());
                adminDao = new AdminDao();
                applicationDao = new ApplicationDao();
        }

        @Override
        protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken) {
                WorkspaceAuthenticationToken token = (WorkspaceAuthenticationToken) authcToken;

                if (isValidToken(token)) {
                        String principal = token.getUsername();
                        String credentials = getCredentials(token);
                        if (credentials != null) {
                                return new WorkspaceAuthenticationInfo(principal, credentials, getName(), token.getWorkspace());
                        }
                }

                return null;
        }

        @Override
        protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
                Set<String> roleNames = getRoleNames(principals);
                return new SimpleAuthorizationInfo(roleNames);
        }

        private Set<String> getRoleNames(PrincipalCollection principals) {
                Set<String> roleNames = new HashSet<String>();
                Account account = getAccount(principals);
                if (account != null) {
                        if (AdminUtil.isAdmin(account)) {
                                roleNames.add(AccessRole.SC1.name());
                        } else if (DeviceUtil.isDevice(account)) {
                                roleNames.add(AccessRole.SC2.name());
                        }
                }
                return roleNames;
        }

        protected Account getAccount(PrincipalCollection principals) {
                try {
                        WorkspacePrincipalCollection wpc = (WorkspacePrincipalCollection) principals;
                        String usernameOrSerial = (String) wpc.getPrimaryPrincipal();
                        Application application = applicationDao.read(wpc.getWorkspace());
                        return adminDao.readByUsername(application, usernameOrSerial);
                } catch (AccountNotFoundException e) {
                        logger.error(e.getMessage());
                }
                return null;
        }

        private boolean isValidToken(WorkspaceAuthenticationToken token) {
                return token != null && StringUtils.isNoneBlank(token.getWorkspace());
        }

        protected abstract String getCredentials(WorkspaceAuthenticationToken token);
}
