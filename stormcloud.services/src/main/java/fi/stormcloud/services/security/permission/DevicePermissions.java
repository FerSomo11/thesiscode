package fi.stormcloud.services.security.permission;

import org.apache.shiro.authz.permission.WildcardPermission;

public class DevicePermissions {
        public static final WildcardPermission ALL = new WildcardPermission("devices:*");
        public static final WildcardPermission CREATE = new WildcardPermission("devices:create");
        public static final WildcardPermission READ = new WildcardPermission("devices:read");
        public static final WildcardPermission UPDATE = new WildcardPermission("devices:update");
        public static final WildcardPermission DELETE = new WildcardPermission("devices:delete");
}
