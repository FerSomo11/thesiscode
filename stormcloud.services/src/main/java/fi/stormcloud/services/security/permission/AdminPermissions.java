package fi.stormcloud.services.security.permission;

import org.apache.shiro.authz.permission.WildcardPermission;

public class AdminPermissions {
        public static final WildcardPermission ALL = new WildcardPermission("admins:*");
        public static final WildcardPermission CREATE = new WildcardPermission("admins:create");
        public static final WildcardPermission READ = new WildcardPermission("admins:read");
        public static final WildcardPermission UPDATE = new WildcardPermission("admins:update");
        public static final WildcardPermission DELETE = new WildcardPermission("admins:delete");
}
