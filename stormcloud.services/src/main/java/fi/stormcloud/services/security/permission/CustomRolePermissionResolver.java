package fi.stormcloud.services.security.permission;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.authz.permission.RolePermissionResolver;

import fi.stormcloud.services.security.realm.BaseRealm.AccessRole;

public class CustomRolePermissionResolver implements RolePermissionResolver {

        // private final static Logger logger = LoggerFactory.getLogger(CustomRolePermissionResolver.class);

        @Override
        public Collection<Permission> resolvePermissionsInRole(String roleString) {
                if (StringUtils.equals(roleString, AccessRole.SC1.name())) {
                        return getAdminPermissions();
                }

                if (StringUtils.equals(roleString, AccessRole.SC2.name())) {
                        return getDevicePermissions();
                }
                return CollectionUtils.emptyCollection();
        }

        private Collection<Permission> getAdminPermissions() {
                List<Permission> perms = new ArrayList<Permission>();
                perms.add(AdminPermissions.ALL);
                perms.add(DevicePermissions.ALL);
                perms.add(DataPermissions.PULL);
                return perms;
        }

        private Collection<Permission> getDevicePermissions() {
                List<Permission> perms = new ArrayList<Permission>();
                perms.add(DataPermissions.ALL);
                return perms;

        }
}
