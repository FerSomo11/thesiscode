package fi.stormcloud.services.security.permission;

import org.apache.shiro.authz.permission.WildcardPermission;

public class DataPermissions {
        public static final WildcardPermission ALL = new WildcardPermission("data:*");
        public static final WildcardPermission PUSH = new WildcardPermission("data:push");
        public static final WildcardPermission PULL = new WildcardPermission("data:pull");
       
}
