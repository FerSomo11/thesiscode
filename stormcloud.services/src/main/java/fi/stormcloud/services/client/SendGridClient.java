package fi.stormcloud.services.client;

import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sendgrid.SendGrid;

public class SendGridClient extends BaseClient {

        private final static Logger logger = LoggerFactory.getLogger(SendGridClient.class);

        public static final SendGrid INSTANCE;

        static {
                try {
                        INSTANCE = buildSendgridClient();
                } catch (Exception e) {
                        logger.error(e.getMessage());
                        throw new ExceptionInInitializerError(e);
                }
        }

        private static SendGrid buildSendgridClient() throws IOException {
                Properties props = loadProperties();
                return new SendGrid(props.getProperty("sendgrid.username"), props.getProperty("sendgrid.password"));
        }
}
