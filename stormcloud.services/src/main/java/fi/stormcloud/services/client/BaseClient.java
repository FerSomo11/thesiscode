package fi.stormcloud.services.client;

import java.io.IOException;
import java.util.Properties;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

public abstract class BaseClient {
        private static final String RESOURCE_PATH = "fi/stormcloud/services/props/clients.properties";

        protected static Properties loadProperties() throws IOException {
                return PropertiesLoaderUtils.loadProperties(new ClassPathResource(RESOURCE_PATH));
        }
}
