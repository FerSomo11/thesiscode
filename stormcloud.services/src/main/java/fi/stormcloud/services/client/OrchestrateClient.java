package fi.stormcloud.services.client;

import io.orchestrate.client.Client;

import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OrchestrateClient extends BaseClient {

        private final static Logger logger = LoggerFactory.getLogger(OrchestrateClient.class);

        public static final Client INSTANCE;

        static {
                try {
                        INSTANCE = buildOrchestrateClient();
                } catch (Exception e) {
                        logger.error(e.getMessage());
                        throw new ExceptionInInitializerError(e);
                }
        }

        private static Client buildOrchestrateClient() throws IOException {
                Properties props = loadProperties();
                return io.orchestrate.client.OrchestrateClient.builder(props.getProperty("orchestrate.apiKey")).host(props.getProperty("orchestrate.host")).build();
        }
}
