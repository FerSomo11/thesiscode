package fi.stormcloud.services.client;

import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.stormpath.sdk.api.ApiKey;
import com.stormpath.sdk.api.ApiKeys;
import com.stormpath.sdk.client.Client;
import com.stormpath.sdk.client.Clients;
import com.stormpath.sdk.impl.cache.DefaultCacheManager;

public class StormpathClient extends BaseClient {

        private final static Logger logger = LoggerFactory.getLogger(StormpathClient.class);

        public static final Client INSTANCE;

        static {
                try {
                        INSTANCE = buildStormpathClient();
                } catch (Exception e) {
                        logger.error(e.getMessage());
                        throw new ExceptionInInitializerError(e);
                }
        }

        private static Client buildStormpathClient() throws IOException {
                Properties props = loadProperties();
                ApiKey apiKey = ApiKeys.builder().setIdPropertyName("stormpath.id").setSecretPropertyName("stormpath.secret").setProperties(props).build();
                return Clients.builder().setApiKey(apiKey).setCacheManager(new DefaultCacheManager()).build();
        }
}
