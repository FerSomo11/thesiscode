package fi.stormcloud.core.model;

public class Location {

        private Double longitude;
        private Double latitude;

        public Location(Double latitude, Double longitude) {
                this.latitude = latitude;
                this.longitude = longitude;
        }

        public Double getLongitude() {
                return longitude;
        }

        public Double getLatitude() {
                return latitude;
        }

}
