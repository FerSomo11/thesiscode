package fi.stormcloud.core.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Device {
        private String deviceId;
        private String serial;
        private String tag;
        private String description;
        private String email;
        private Boolean enable;

        private String createdBy;
        private String modifiedBy;

        private String createdAt;
        private String modifiedAt;

        public String getDeviceId() {
                return deviceId;
        }

        public void setDeviceId(String id) {
                this.deviceId = id;
        }

        public String getEmail() {
                return email;
        }

        public void setEmail(String email) {
                this.email = email;
        }

        public Boolean getEnable() {
                return enable != null && enable;
        }

        public void setEnable(Boolean enable) {
                this.enable = enable != null && enable;
        }

        public String getSerial() {
                return serial;
        }

        public void setSerial(String serial) {
                this.serial = serial;
        }

        public String getTag() {
                return tag;
        }

        public void setTag(String tag) {
                this.tag = tag;
        }

        public String getDescription() {
                return description;
        }

        public void setDescription(String description) {
                this.description = description;
        }

        public String getCreatedBy() {
                return createdBy;
        }

        public void setCreatedBy(String createdBy) {
                this.createdBy = createdBy;
        }

        public String getModifiedBy() {
                return modifiedBy;
        }

        public void setModifiedBy(String modifiedBy) {
                this.modifiedBy = modifiedBy;
        }

        public String getCreatedAt() {
                return createdAt;
        }

        public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
        }

        public String getModifiedAt() {
                return modifiedAt;
        }

        public void setModifiedAt(String modifiedAt) {
                this.modifiedAt = modifiedAt;
        }
}
